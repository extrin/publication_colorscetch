#include <iostream>
#include <math.h>
#include <emmintrin.h>
#include "../../CImg.h"
using namespace cimg_library;
using namespace std;

//���������
const char* imageFileName = "D:\\car.bmp";
int HT = 3000;
int CT = 1000;
int threshold = 4;
#define NUM_THREADS 7 //���������� �������

const unsigned char umask1[16] = { 0, 2, 4, 6, 8, 10, 12, 14, 1, 3, 5, 7, 9, 11, 13, 15 };		//����� ��� ������������ u16b �� ������� ����� 0-7
const unsigned char umask2[16] = { 1, 3, 5, 7, 9, 11, 13, 15, 0, 2, 4, 6, 8, 10, 12, 14 };		//����� ��� ������������ u16b �� ������� ����� 8-15
const __m128i mask1 = _mm_loadu_si128((__m128i*)(umask1));					//��������� �������� ����� � ������ __m128i
const __m128i mask2 = _mm_loadu_si128((__m128i*)(umask2));


void SaveImage(unsigned char *matrixR, unsigned char *matrixG, unsigned char *matrixB, const int H, const int W, const int L, const char* string)
{
	CImg<unsigned char> imageFilter(W, H, 1, 3);//������� ����������� � ����������� ���������
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			const unsigned char Color[] = { matrixR[y*W + x], matrixG[y*W + x], matrixB[y*W + x] };//������� ���� ������� (x,y)
			imageFilter.draw_point(x, y, Color);//����������� ������� ������ ����������� ���� ������
		}
	imageFilter.save(string);
}

inline __m128i two_8b_to_16b(__m128i first, __m128i second)
{
	first = _mm_shuffle_epi8(first, mask1);
	second = _mm_shuffle_epi8(second, mask2);
	return   _mm_add_epi8(first, second);
}

void GetGray(unsigned char* Red, unsigned char* Green, unsigned char* Blue, unsigned char* Gray, const int Count)
{
	__m128i zero = _mm_setzero_si128();
	__m128i red_buf, green_buf, blue_buf, buf;
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int x = 0; x<Count; x += sizeof(__m128i))
	{
		__m128i sum1 = _mm_setzero_si128();
		__m128i sum2 = _mm_setzero_si128();

		red_buf = _mm_loadu_si128((__m128i*)(Red + x));				//��������� 16 1b ��������
		buf = _mm_cvtepu8_epi16(red_buf);					//������� ������ 8 ����� 1b � 2b
		sum1 = _mm_adds_epi16(sum1, buf);						//�������� ������ 8 2b �����
		buf = _mm_unpackhi_epi8(red_buf, zero);			//������� ������ 8 ����� 1b � 2b
		sum2 = _mm_adds_epi16(sum2, buf);						//�������� ������ 8 2b �����	

		green_buf = _mm_loadu_si128((__m128i*)(Green + x));
		buf = _mm_cvtepu8_epi16(green_buf);
		sum1 = _mm_adds_epi16(sum1, buf);
		sum1 = _mm_adds_epi16(sum1, buf);
		buf = _mm_unpackhi_epi8(green_buf, zero);
		sum2 = _mm_adds_epi16(sum2, buf);
		sum2 = _mm_adds_epi16(sum2, buf);

		blue_buf = _mm_loadu_si128((__m128i*)(Blue + x));
		buf = _mm_cvtepu8_epi16(blue_buf);
		sum1 = _mm_adds_epi16(sum1, buf);
		buf = _mm_unpackhi_epi8(blue_buf, zero);
		sum2 = _mm_adds_epi16(sum2, buf);

		sum1 = _mm_srai_epi16(sum1, 2);
		sum2 = _mm_srai_epi16(sum2, 2);
		_mm_storeu_si128((__m128i*)(Gray + x), two_8b_to_16b(sum1, sum2));						//�������� �������� �� ������
	}
}

void GlobalContrastCorrection(unsigned char* Gray, const int Height, const int Width, const int count, int HT, int CT)
{
	int Hist[256];
	int left, right;
	for (int y = 0; y<256; ++y) Hist[y] = 0;

	for (int y = 0; y<count; ++y) ++Hist[Gray[y]];
	int lsum = 0, rsum = 0;
	for (left = 0; left<100; ++left)
	{
		if (Hist[left] >HT) break;
		lsum += Hist[left];
		if (lsum > CT) break;
	}
	for (right = 255; right>150; --right)
	{
		if (Hist[right]>HT) break;
		rsum += Hist[right];
		if (rsum > CT) break;
	}
	float GCC[256];
	for (int i = 0; i<left; ++i)			GCC[i] = 0;
	for (int i = left; i<right; ++i)		GCC[i] = 255 * (i - left) / (right - left);
	for (int i = right; i<256; ++i)			GCC[i] = 255;
	for (int y = 0; y<count; ++y)  Gray[y] = GCC[Gray[y]];
}

void DoB(unsigned char* Gray, const int Height, const int Width, const int Count, int threshold)
{
	unsigned int* IntImage = new unsigned int[Count];
	IntImage[0] = Gray[0];
	for (int x = 1; x<Width; ++x)
		IntImage[x] = Gray[x] + IntImage[x - 1];
	for (int x = 1; x<Height; ++x)
		IntImage[x*Width] = Gray[x*Width] + IntImage[(x - 1)*Width];
	for (int y = 1; y<Height; ++y)
		for (int x = 1; x<Width; ++x)
			IntImage[x + y*Width] = Gray[x + y*Width] + IntImage[x + y*Width - 1] + IntImage[x + (y - 1)*Width] - IntImage[x + (y - 1)*Width - 1];

	__m128i sum1, sum2;
	__m64 dog;
	float div1 = 25.0f, div2 = 9.0f;
	__m64 ed = _mm_set1_pi16(1), thr = _mm_set1_pi16(threshold), max = _mm_set1_pi16(255);
	__m64 mask = _mm_set_pi8(7, 5, 3, 1, 6, 4, 2, 0);

#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; x += 4)
		{
			//sum1=_mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+3)*BMPinf.Width+3)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-4)*BMPinf.Width-4))),_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+3)*BMPinf.Width-4)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-4)*BMPinf.Width+3)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 7�7
			sum1 = _mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage + x + (y + 2)*Width + 2)), _mm_loadu_si128((__m128i*)(IntImage + x + (y - 3)*Width - 3))), _mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage + x + (y + 2)*Width - 3)), _mm_loadu_si128((__m128i*)(IntImage + x + (y - 3)*Width + 2)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 5�5
			sum2 = _mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage + x + (y + 1)*Width + 1)), _mm_loadu_si128((__m128i*)(IntImage + x + (y - 2)*Width - 2))), _mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage + x + (y + 1)*Width - 2)), _mm_loadu_si128((__m128i*)(IntImage + x + (y - 2)*Width + 1)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 3�3
			dog = _mm_cvtps_pi16(_mm_sub_ps(_mm_div_ps(_mm_cvtepi32_ps(sum2), _mm_load1_ps(&div2)), _mm_div_ps(_mm_cvtepi32_ps(sum1), _mm_load1_ps(&div1))));//�������������� � �����-> ������� -> ��������� -> �������������� � __m64
			dog = _mm_cmpgt_pi16(thr, dog);//c��������� � �������; 0 - ������ ������, -1  - ������ ������
			dog = _mm_add_pi16(dog, ed);		 //���������� 1 - �������� ����� � �������: 1 - �������� ������ ������, 0 - �������� ������ ������
			//dog=_mm_mullo_pi16(max,dog);	 //�������� ������������ ������ (255) �� �����
			dog = _mm_shuffle_pi8(dog, mask);  //������ ������������ �������� �� ������ 4 �������
			_mm_stream_pi((__m64*)(Gray + x + y*Width), dog); // ������ � Gray
		}

}

void Closing(unsigned char* Gray, const int Height, const int Width, const int Count)
{

	const int n = 3;
	__m128i zero = _mm_setzero_si128(), ed = _mm_set1_epi8(1);
	__m128i* mas8[n*n];
	int mas[n*n] = { 0, 1, 0,
		1, 1, 1,
		0, 1, 0 };
	int sum = 0;
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i<n*n; ++i)
		if (mas[i] == 0) mas8[i] = &zero; else
		{
			++sum;
			mas8[i] = &ed;
		}
	__m128i sum8 = _mm_set1_epi8(sum);
	int size = n / 2;
	unsigned char* Buf = new unsigned char[Count];
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; x += sizeof(__m128i))
		{
			__m128i s = _mm_setzero_si128();
			for (int d = -size; d<size + 1; ++d)
				for (int c = -size; c<size + 1; ++c)
				{
					__m128i gray16 = _mm_loadu_si128((__m128i*)(Gray + x + (y + d)*Width + c));		//��������
					gray16 = _mm_and_si128(gray16, *mas8[c + n*(d + size) + size]);						    //���������� ���������
					s = _mm_add_epi8(s, gray16);														//�����
				}
			//s=_mm_cmpgt_epi8(s,zero);��� ������ 255 ��������
			s = _mm_add_epi8(_mm_cmpgt_epi8(ed, s), ed);
			_mm_storeu_si128((__m128i*)(Buf + x + y*Width), s);
		}
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; x += 16)
		{
			__m128i s = _mm_setzero_si128();
			for (int d = -size; d<size + 1; ++d)
				for (int c = -size; c<size + 1; ++c)
				{
					__m128i gray16 = _mm_loadu_si128((__m128i*)(Buf + x + (y + d)*Width + c));		//��������
					gray16 = _mm_and_si128(gray16, *mas8[c + n*(d + size) + size]);						//���������� ���������
					s = _mm_add_epi8(s, gray16);													//�����
				}
			s = _mm_add_epi8(_mm_cmpgt_epi8(sum8, s), ed);
			_mm_storeu_si128((__m128i*)(Gray + x + y*Width), s);
		}

	/*for (int y=4;y<BMPinf.Height-3;++y)
	for (int x=4;x<BMPinf.Width-3;x++)
	{
	if (Gray[x+y*BMPinf.Width]==1) Gray[x+y*BMPinf.Width]=255;
	}*/
	//		
}

void Filling(unsigned char* Gray, const int Height, const int Width, const int Count)
{
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 4; y<Height - 4; y++)
		for (int x = 4; x < Width - 4; x++)
		{
			bool flag = false;
			for (int l = -4; l < 5; l++)
			{
				if (Gray[(y + l)*Width + (x + 4)] != 0)
					flag = true;
				if (Gray[(y + l)*Width + (x - 4)] != 0)
					flag = true;
				if (Gray[(y + 4)*Width + (x + l)] != 0)
					flag = true;
				if (Gray[(y - 4)*Width + (x + l)] != 0)
					flag = true;
			}
			if (!flag)
				for (int l = -4; l < 5; l++)
					for (int t = -4; t < 5; t++)
						Gray[(y + l)*Width + (x + t)] = 0;
		}
#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < Height; y++)
		for (int x = 0; x < Width; x++)
			if (Gray[x + y*Width] == 1) Gray[x + y*Width] = 255;
}

void Blending(unsigned char* Red, unsigned char* Green, unsigned char* Blue, unsigned char* Gray, const int Height, const int Width, const int Count)
{
	__m128i buf, gr;
	__m128i byteEd = _mm_set1_epi8(255); // ������������� ������ 111..11
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; x += sizeof(__m128i))
		{
			gr = _mm_loadu_si128((__m128i*)(Gray + x + y*Width));
			gr = _mm_xor_si128(gr, byteEd);												//xor c 111..11 �� ��� �������� not
			buf = _mm_loadu_si128((__m128i*)(Blue + x + y*Width));
			_mm_storeu_si128((__m128i*)(Blue + x + y*Width), _mm_or_si128(buf, gr));	//���������� ��������
			buf = _mm_loadu_si128((__m128i*)(Green + x + y*Width));
			_mm_storeu_si128((__m128i*)(Green + x + y*Width), _mm_or_si128(buf, gr));
			buf = _mm_loadu_si128((__m128i*)(Red + x + y*Width));
			_mm_storeu_si128((__m128i*)(Red + x + y*Width), _mm_or_si128(buf, gr));
		}
}

int main()
{
	CImg<unsigned char> image(imageFileName);
	const int Height = image.height();
	const int Width = image.width();
	const int Count = Height*Width;
	unsigned char* Gray = new unsigned char[Count];
	unsigned char* Red = new unsigned char[Count];
	unsigned char* Green = new unsigned char[Count];
	unsigned char* Blue = new unsigned char[Count];
	for (int y = 0; y<Height; y++)
		for (int x = 0; x<Width; x++)
		{
			int r = image(x, y, 0);
			int g = image(x, y, 1);
			int b = image(x, y, 2);
			Red[y*Width + x] = r;
			Green[y*Width + x] = g;
			Blue[y*Width + x] = b;
		}
	LARGE_INTEGER frequency, start, finish;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	GetGray(Red, Green, Blue, Gray, Count);
	GlobalContrastCorrection(Gray, Height, Width, Count, HT, CT);
	DoB(Gray, Height, Width, Count, threshold);
	Closing(Gray, Height, Width, Count);
	Filling(Gray, Height, Width, Count);
	Blending(Red, Green, Blue, Gray, Height, Width, Count);

	QueryPerformanceCounter(&finish);
	int tick = (__int64)(finish.QuadPart - start.QuadPart);
	int frq = (__int64)(frequency.QuadPart);
	//cout << "QPC = " << tick << " / " << frq << " ms" << endl;
	printf("QPC = %0.3f ms\n", double(finish.QuadPart - start.QuadPart)/frequency.QuadPart*1000);

	SaveImage(Red, Green, Blue, Height, Width, Count, "D:\\car_out_simd2.bmp");
	system("pause");
	return 0;
}