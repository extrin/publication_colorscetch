// OpenMP_project_naivn.cpp: ���������� ����� ����� ��� ����������� ����������.
//

// OpenMP_project.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include "../../CImg.h"

using namespace cimg_library;
using namespace std;

#define NUM_MEAN_1 5 //������ ����� �������� ������� ��� ��������
#define NUM_MEAN_2 3 //������ ����� �������� ������� ��� ��������
#define STEP 2   //����� (���������) 2 ��� � �������


void AutoContrastGray(unsigned char*, const int, const int, const int, int, int);
void Closing(unsigned char*, const int, const int, const int);
void DoB(unsigned char*, const int, const int, const int);
void DeleteSS9(unsigned char*, const int, const int, const int);
void SaveImage(unsigned char*, unsigned char*, unsigned char*, const int, const int, const int, const char*);

int main()
{
	const char* imageFileName = "D:\\car.bmp";
	CImg<unsigned char> image(imageFileName);//�������� �����������
	const int H = image.height();
	const int W = image.width();

	const int L = H*W;//���������� �������� �����������
	unsigned char* grayscale_image = new unsigned char[L];//����������� � ��������� ������
	bool* Ind = new bool[L];
	unsigned char* R = new unsigned char[L];
	unsigned char* G = new unsigned char[L];
	unsigned char* B = new unsigned char[L];
	memset(Ind, false, L);//��������� ������ Ind ���������� false

	LARGE_INTEGER frequency, start, finish;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	for (int y = 0; y<H; y++)
		for (int x = 0; x<W; x++)
		{
			int r = image(x, y, 0);
			int g = image(x, y, 1);
			int b = image(x, y, 2);
			R[y*W + x] = r;
			G[y*W + x] = g;
			B[y*W + x] = b;
			grayscale_image[y*W + x] = (int)(0.3*r + 0.6*g + 0.1*b);//��������� � �������� ������
		}

	AutoContrastGray(grayscale_image, H, W, L, 3000, 1000);//��������������������
	DoB(grayscale_image, H, W, L);//���� �������
	Closing(grayscale_image, H, W, L);//�������� �������� (������� ������� �� �������� ��������)
	DeleteSS9(grayscale_image, H, W, L);

	for (int i = 0; i<L; i++)
	{
		if (grayscale_image[i] == 0)
		{
			R[i] = 255;
			G[i] = 255;
			B[i] = 255;
		}
	}

	QueryPerformanceCounter(&finish);
	// ����� � �������������
	float delay = (finish.QuadPart - start.QuadPart) * 1000.0f / frequency.QuadPart;
	printf("QueryPerformanceCounter = %0.3f ms\n", delay);

	SaveImage(R, G, B, H, W, L, "D:\\image_result.bmp");//�������� �����������
	delete[] grayscale_image;
	grayscale_image = nullptr;
	///
	delete[] Ind;
	Ind = nullptr;
	///
	delete[] R;
	R = nullptr;
	///
	delete[] G;
	G = nullptr;
	///
	delete[] B;
	B = nullptr;

	system("pause");
	return 0;
}

//-----------------------------------------------------------------------------------------
//                    ��������� ����������� ��������� �����������                         
//-----------------------------------------------------------------------------------------
void AutoContrastGray(unsigned char* Matrix, const int H, const int W, const int L, int HT, int CT)
{

	unsigned long alHist[256], lT = 120, lSum;
	unsigned char abLUT[256];
	int i, lLeftLim, lRightLim;
	// ���������� ����������� ��������
	memset(alHist, 0, 256 * sizeof(long));
	unsigned char *pb = Matrix;

	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			alHist[pb[y*W + x]]++;
		}

	// ���������� ����� ������� ��� ����������������
	lSum = 0;
	for (lLeftLim = 0; lLeftLim < 100; lLeftLim++)
	{

		if (alHist[lLeftLim] > HT) break;
		lSum += alHist[lLeftLim];

		if (lSum > CT) break;
	}
	// ���������� ������ ������� ��� ����������������
	lSum = 0;
	for (lRightLim = 255; lRightLim > 150; lRightLim--)
	{
		if (alHist[lRightLim] > lT) break;
		lSum += alHist[lRightLim];
		if (lSum > lT) break;
	}
	// ���������� ������� ������������� (LUT, Look-Up Table)
	for (i = 0; i < lLeftLim; i++) { abLUT[i] = (unsigned char)0; }
	for (i = lLeftLim; i < lRightLim; i++)
	{
		abLUT[i] = (unsigned char)(255 * (i - lLeftLim) / (lRightLim - lLeftLim));
	}
	for (i = lRightLim; i < 256; i++) { abLUT[i] = (unsigned char)255; }
	// ��������������� ����������������

	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			pb[y*W + x] = abLUT[pb[y*W + x]];
		}
}

//-----------------------------------------------------------------------------------------
//                             ������� �������
//-----------------------------------------------------------------------------------------
void DoB(unsigned char* Matrix, const int H, const int W, const int L)
{

	int *integral = new int[L];

	for (int i = 0; i<L; i++)
		integral[i] = (int)Matrix[i]; //�������� �� Matrix � integral
	//��������� ������������ �����������
	// ����� �� ���������

	for (int x = 0; x < W; x++)
		for (int y = 1; y < H; y++)
		{
			integral[y*W + x] += integral[(y - 1)*W + x];
		}
	//����� �� �������

	for (int y = 0; y < H; y++)
		for (int x = 1; x < W; x++)
		{
			integral[y*W + x] += integral[y*W + (x - 1)];
		}

	//������� �������
	double* first_mean = new double[L];//���������� ������� ������ ������
	double* second_mean = new double[L];

	int hs_1 = NUM_MEAN_1 / 2;//�������� �� ������� ����� (������������� �������)
	int hs_2 = NUM_MEAN_2 / 2;
	//������ �������
	for (int y = hs_1; y<H - hs_1; y++)//��� ���� �������� �����������
		for (int x = hs_1; x<W - hs_1; x++)
		{
			int sum = 0;
			if (y - hs_1 >= 1 && x - hs_1 >= 1)
				sum = integral[(y + hs_1)*W + (x + hs_1)] + integral[(y - hs_1 - 1)*W + (x - hs_1 - 1)] - integral[(y + hs_1)*W + (x - hs_1 - 1)]
				- integral[(y - hs_1 - 1)*W + (x + hs_1)];
			else
				sum = integral[(y + hs_1)*W + (x + hs_1)];
			double num1 = NUM_MEAN_1*NUM_MEAN_1;
			first_mean[y*W + x] = (double)sum / num1;
		}
	//������ �������
	for (int y = hs_2; y<H - hs_2; y++)//��� ���� �������� �����������
		for (int x = hs_2; x<W - hs_2; x++)
		{
			int sum = 0;
			if (y - hs_2 >= 1 && x - hs_2 >= 1)
				sum = integral[(y + hs_2)*W + (x + hs_2)] + integral[(y - hs_2 - 1)*W + (x - hs_2 - 1)] - integral[(y + hs_2)*W + (x - hs_2 - 1)]
				- integral[(y - hs_2 - 1)*W + (x + hs_2)];
			else
				sum = integral[(y + hs_2)*W + (x + hs_2)];
			double num2 = NUM_MEAN_2*NUM_MEAN_2;
			second_mean[y*W + x] = (double)sum / num2;
		}

	for (int y = 0; y < H; y++)//�������� �� ����� �����������
		for (int x = 0; x < W; x++)
		{
			int p = second_mean[y*W + x] - first_mean[y*W + x];
			if (p<0) p = 0;
			if (p>255) p = 255;
			Matrix[y*W + x] = p>STEP ? 255 : 0;
		}

	//������� ��������
	delete[] first_mean;
	first_mean = nullptr;
	//////
	delete[] second_mean;
	second_mean = nullptr;
	//////
	delete[] integral;
	integral = nullptr;
}
//-----------------------------------------------------------------------------------------
//                             �������� ��������
//-----------------------------------------------------------------------------------------
void Closing(unsigned char* Matrix, const int H, const int W, const int L)
{
	unsigned char* temp_arr = new unsigned char[L];
	memset(temp_arr, 0, L);//temp_arr[:]=0
	//������ 3�3
	//���������

	for (int y = 1; y<H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (Matrix[y*W + x] == 255)
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						if (l == 0 || t == 0)
							temp_arr[(y + l)*W + (x + t)] = 255;//������� ��������� �� ��������� ������
		}

	//������

	for (int y = 1; y < H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (temp_arr[y*W + x] == 0)
				Matrix[y*W + x] = 0;
			else
			{
				Matrix[y*W + x] = 255;
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						if (l == 0 || t == 0)
							if (temp_arr[(y + l)*W + (x + t)] == 0)
							{
								Matrix[y*W + x] = 0;
								break;
							}
			}
		}

	//������� ��������
	delete[] temp_arr;
	temp_arr = nullptr;
}
//-----------------------------------------------------------------------------------------
//                         �������� ��������� �������� ���������� �����
//-----------------------------------------------------------------------------------------
void DeleteSS9(unsigned char* Matrix, const int H, const int W, const int L)
{
	//������ �����=9 � 9

	for (int y = 4; y<H - 4; y++)
		for (int x = 4; x < W - 4; x++)
		{
			bool flag = false;
			for (int l = -4; l < 5; l++)
			{
				if (Matrix[(y + l)*W + (x + 4)] != 0)
					flag = true;
				if (Matrix[(y + l)*W + (x - 4)] != 0)
					flag = true;
				if (Matrix[(y + 4)*W + (x + l)] != 0)
					flag = true;
				if (Matrix[(y - 4)*W + (x + l)] != 0)
					flag = true;
			}
			if (!flag)
				for (int l = -4; l < 5; l++)
					for (int t = -4; t < 5; t++)
						Matrix[(y + l)*W + (x + t)] = 0;
		}
}

//-----------------------------------------------------------------------------------------
//                         ���������� �����������
//-----------------------------------------------------------------------------------------
void SaveImage(unsigned char *matrixR, unsigned char *matrixG, unsigned char *matrixB, const int H, const int W, const int L, const char* string)
{
	CImg<unsigned char> imageFilter(W, H, 1, 3);//������� ����������� � ����������� ���������
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			const unsigned char Color[] = { matrixR[y*W + x], matrixG[y*W + x], matrixB[y*W + x] };//������� ���� ������� (x,y)
			imageFilter.draw_point(x, y, Color);//����������� ������� ������ ����������� ���� ������
		}

	imageFilter.save(string);
}