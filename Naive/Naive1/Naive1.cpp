#include <iostream>
#include <math.h>
#include "../../Cimg.h"
using namespace cimg_library;
using namespace std;

//���������
const char* imageFileName = "D:\\car.bmp";
int HT = 3000;
int CT = 1000;

int threshold = 4;

void SaveImage(unsigned char *matrixR, unsigned char *matrixG, unsigned char *matrixB, const int H, const int W, const int L, const char* string)
{
	CImg<unsigned char> imageFilter(W, H, 1, 3);//������� ����������� � ����������� ���������

	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			const unsigned char Color[] = { matrixR[y*W + x], matrixG[y*W + x], matrixB[y*W + x] };//������� ���� ������� (x,y)
			imageFilter.draw_point(x, y, Color);//����������� ������� ������ ����������� ���� ������
		}
	imageFilter.save(string);
}

void GetGray(unsigned char* Red, unsigned char* Green, unsigned char* Blue, unsigned char* Gray, const int Count)
{
	for (unsigned int x = 0; x<Count; ++x)
		Gray[x] = 0.3*Red[x] + 0.6*Green[x] + 0.1*Blue[x];
}

void GlobalContrastCorrection(unsigned char* Gray, const int Height, const int Width, const int count, int HT, int CT)
{
	int Hist[256];
	int left, right;
	for (int y = 0; y<256; ++y) Hist[y] = 0;
	for (int y = 0; y<count; ++y) ++Hist[Gray[y]];
	int lsum = 0, rsum = 0;
	for (left = 0; left<100; ++left)
	{
		if (Hist[left] >HT) break;
		lsum += Hist[left];
		if (lsum > CT) break;
	}
	for (right = 255; right>150; --right)
	{
		if (Hist[right]>HT) break;
		rsum += Hist[right];
		if (rsum > CT) break;
	}
	float GCC[256];
	for (int i = 0; i<left; ++i)            GCC[i] = 0;
	for (int i = left; i<right; ++i)        GCC[i] = 255 * (i - left) / (right - left);
	for (int i = right; i<256; ++i)         GCC[i] = 255;
	for (unsigned int y = 0; y<count; ++y)  Gray[y] = GCC[Gray[y]];
}

void DoB(unsigned char* Gray, const int Height, const int Width, const int Count, int threshold)
{
	unsigned int* IntImage = new unsigned int[Count];
	IntImage[0] = Gray[0];
	for (int x = 1; x<Width; ++x)
		IntImage[x] = Gray[x] + IntImage[x - 1];
	for (int x = 1; x<Height; ++x)
		IntImage[x*Width] = Gray[x*Width] + IntImage[(x - 1)*Width];
	for (int y = 1; y<Height; ++y)
		for (int x = 1; x<Width; ++x)
			IntImage[x + y*Width] = Gray[x + y*Width] + IntImage[x + y*Width - 1] + IntImage[x + (y - 1)*Width] - IntImage[x + (y - 1)*Width - 1];
	float z1, z2;
	int thr = 5;
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; ++x)
		{
			//z1=IntImage[x+(y+3)*BMPinf.Width+3]-IntImage[x+(y+3)*BMPinf.Width-4]+IntImage[x+(y-4)*BMPinf.Width-4]-IntImage[x+(y-4)*BMPinf.Width+3];
			z1 = IntImage[x + (y + 2)*Width + 2] - IntImage[x + (y + 2)*Width - 3] + IntImage[x + (y - 3)*Width - 3] - IntImage[x + (y - 3)*Width + 2];
			z2 = IntImage[x + (y + 1)*Width + 1] - IntImage[x + (y + 1)*Width - 2] + IntImage[x + (y - 2)*Width - 2] - IntImage[x + (y - 2)*Width + 1];
			z1 = z2 / 9. - z1 / 25.;
			if (z1<threshold)  z1 = 0;
			if (z1 >= threshold) z1 = 1;
			Gray[x + y*Width] = (int)(z1 + 0.5);
		}
}

void Closing(unsigned char* Gray, const int Height, const int Width, const int Count)
{
	unsigned char* Buf = new unsigned char[Count];
	int mas[9] = { 0, 1, 0,
		1, 1, 1,
		0, 1, 0 };
	int n = 3, sum = 0;
	for (int i = 0; i<n*n; ++i) if (mas[i]>0) ++sum;
	int size = n / 2;
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; ++x)
		{
			int s = 0;
			for (int d = -size; d<size + 1; ++d)
				for (int c = -size; c<size + 1; ++c)
					s += Gray[x + (y + d)*Width + c] * mas[c + n*(d + size) + size];
			if (s>0) Buf[x + y*Width] = 1; else Buf[x + y*Width] = 0;
		}
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; ++x)
		{
			int s = 0;
			for (int d = -size; d<size + 1; ++d)
				for (int c = -size; c<size + 1; ++c)
					s += Buf[x + (y + d)*Width + c] * mas[c + n*(d + size) + size];
			if (s<sum) Gray[x + y*Width] = 0; else Gray[x + y*Width] = 1;
		}
}

void Filling(unsigned char* Gray, const int Height, const int Width, const int Count)
{
	int s, size = 7;
	int hsize = (size + 1) / 2;
	for (int y = 4; y<Height - 3; ++y)
	{
		for (int x = 4; x<Width - 3; ++x)
		{
			s = 0;
			for (int j = -hsize; j<hsize; ++j)
			{
				s += Gray[x + (y - hsize)*Width + j];
				s += Gray[x + (y + hsize - 1)*Width + j];
			}
			for (int j = -hsize + 1; j<hsize - 1; ++j)
			{
				s += Gray[x + (y + j)*Width - hsize];
				s += Gray[x + (y + j)*Width + hsize - 1];
			}
			if (s == 0)
				for (int j = -hsize + 1; j<hsize - 1; ++j)
					for (int i = -hsize + 1; i<hsize - 1; ++i)
					{
						Gray[x + i + (y + j)*Width] = 0;
					}
		}
	}
	//for (int y=4;y<Height-3;++y)
	//      for (int x=4;x<Width-3;++x)
	//      {
	//          if (Gray[x+y*Width]==1) Gray[x+y*Width]=255;
	//      }
}

void Blending(unsigned char* Red, unsigned char* Green, unsigned char* Blue, unsigned char* Gray, const int Height, const int Width, const int Count)
{
	for (int y = 4; y<Height - 3; ++y)
		for (int x = 4; x<Width - 3; ++x)
		{
			Red[x + y*Width] *= Gray[x + y*Width];
			Blue[x + y*Width] *= Gray[x + y*Width];
			Green[x + y*Width] *= Gray[x + y*Width];
		}
}

int main()
{
	CImg<unsigned char> image(imageFileName);
	const int Height = image.height();
	const int Width = image.width();
	const int Count = Height*Width;
	unsigned char* Gray = new unsigned char[Count];
	unsigned char* Red = new unsigned char[Count];
	unsigned char* Green = new unsigned char[Count];
	unsigned char* Blue = new unsigned char[Count];
	for (int y = 0; y<Height; y++)
		for (int x = 0; x<Width; x++)
		{
			int r = image(x, y, 0);
			int g = image(x, y, 1);
			int b = image(x, y, 2);
			Red[y*Width + x] = r;
			Green[y*Width + x] = g;
			Blue[y*Width + x] = b;
		}
	LARGE_INTEGER frequency, start, finish;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	GetGray(Red, Green, Blue, Gray, Count);
	GlobalContrastCorrection(Gray, Height, Width, Count, HT, CT);
	DoB(Gray, Height, Width, Count, threshold);
	Closing(Gray, Height, Width, Count);
	Filling(Gray, Height, Width, Count);
	Blending(Red, Green, Blue, Gray, Height, Width, Count);

	QueryPerformanceCounter(&finish);
	cout << "QPC = " << double(finish.QuadPart - start.QuadPart) / frequency.QuadPart * 1000 << " ms" << endl;
	SaveImage(Red, Green, Blue, Height, Width, Count, "D:\\car_out.bmp");
	system("pause");
	return 0;
}