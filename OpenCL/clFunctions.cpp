#include "clFunctions.h"
#define MAX_SOURCE_SIZE (0x100000)

cl_device_id clGetDeviceId() {
	char device_name[80];
	char vendor_name[80];

	cl_device_id device_id = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices, get_num_devices;
	cl_uint ret_num_platforms, get_num_platforms;
	cl_int ret = NULL;

	// ��������� ���������� ��������� �������� � ���������
	ret = clGetPlatformIDs(1, &platform_id, &get_num_platforms);

	// �������� ������ ��������� ��������
	cl_platform_id* platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id)* get_num_platforms);
	ret = clGetPlatformIDs(get_num_platforms, platform_ids, &ret_num_platforms);

	// �������� ����� �������� ��������
	for (int i = 0; i < get_num_platforms; ++i) {
		ret = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 80, vendor_name, NULL);
		printf("%d - %s\n", i, vendor_name);
	}

	int p_id = 0;
	// ���� ���������� ������ ����� ���������, ������ ������� ����
	if (get_num_platforms > 1) {

		do {
			printf("Please input platform ID; default is 0.\nPlatform ID: ");
			scanf_s("%d", &p_id);
		} while (p_id < 0 || p_id >(ret_num_platforms - 1));

	}

	// ������� ���������� ��������� ��������� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, 1, &device_id, &get_num_devices);

	// �������� ������ ��� ������ ���������� ���������
	cl_device_id* device_ids = (cl_device_id*)malloc(sizeof(cl_device_id)* get_num_devices);

	// �������� ������ ���� ��������� OpenCL ��������� ��� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, get_num_devices, device_ids, &ret_num_devices);

	// ������� ������ ���������
	for (int i = 0; i < get_num_devices; ++i) {
		ret = clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 80, device_name, NULL);
		printf("%d - %s\n", i, device_name);
	}

	int dev_id = 0;
	// ���������� ������� ����������, ���� �� ������ 1
	if (get_num_devices > 1) {

		do {
			printf("Please input device ID; default is 0.\nDevice ID: ");
			scanf_s("%d", &dev_id);
			printf("\n");
		} while (dev_id < 0 || dev_id >(ret_num_devices - 1));

	}

	// ������� ���������� � ��������� ����������
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_NAME, 80, device_name, NULL);
	std::cout << "Device Name " << device_name << std::endl;

	cl_uint maxComputeUnits;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &maxComputeUnits, NULL);
	std::cout << "Compute Units " << maxComputeUnits << std::endl;

	cl_uint maxClockFrequency;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &maxClockFrequency, NULL);
	std::cout << "Boost Clock " << maxClockFrequency << "MHz" << std::endl;

	return device_ids[dev_id];
}

int loadKernelFile(const char* fileName, size_t* source_size, char* source_str) {
	// ���������� � �������� kernel
	FILE *fp;

	// �������� ��������� ����, ������������ � kernel
	fopen_s(&fp, fileName, "r");
	if (!fp)
	{
		fprintf(stderr, "Failed to load kernel.\n");
		return 1;
	}

	*source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
	return 0;
}

void oclFinish(cl_command_queue *command_queue) {
	int ret = clFinish(*command_queue);
	switch (ret) {
	case CL_INVALID_COMMAND_QUEUE:
		printf("clFinish command_queue is not a valid command-queue\n");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the host\n");
		break;
	case CL_OUT_OF_RESOURCES:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the device\n");
		break;
	default:
		printf("clFinish the function call was executed successfully code %i\n", ret);
		break;
	}
}

int waitForEventAndRelease(cl_event *event)
{
	cl_int status = CL_SUCCESS;

	status = clWaitForEvents(1, event);

	switch (status) {
	case CL_INVALID_VALUE:
		printf("waitForEventAndRelease num_events is zero\n");
		system("pause");
		break;
	case CL_INVALID_CONTEXT:
		printf("waitForEventAndRelease events specified in event_list do not belong to the same context\n");
		system("pause");
		break;
	case CL_INVALID_EVENT:
		printf("waitForEventAndRelease event objects specified in event_list are not valid event objects\n");
		system("pause");
		break;
	case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
		printf("waitForEventAndRelease the execution status of any of the events in event_list is a negative integer value\n");
		system("pause");
		break;
	case CL_OUT_OF_RESOURCES:
		printf("waitForEventAndRelease there is a failure to allocate resources required by the OpenCL implementation on the device\n");
		system("pause");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		printf("waitForEventAndRelease there is a failure to allocate resources required by the OpenCL implementation on the host\n");
		system("pause");
		break;
	default:
		printf("waitForEventAndRelease the function was executed successfully, status %i\n", status);
		break;
	}

	status = clReleaseEvent(*event);

	return 0;
}

void createBlurMask(float * mask, float sigma, int maskSize) {
	float sum = 0.0f;
	for (int a = -maskSize / 2; a < maskSize / 2 + 1; a++) {
		for (int b = -maskSize / 2; b < maskSize / 2 + 1; b++) {
			float temp = exp(-((float)(a * a + b * b) / (2 * sigma * sigma)));
			sum += temp;
			mask[a + maskSize / 2 + (b + maskSize / 2) * maskSize] = temp;
		}
	}
	// ����������
	for (int i = 0; i < maskSize*maskSize; i++)
		mask[i] = mask[i] / sum;

}