#include "../../CImg.h"
#include "../clFunctions.h"
#include <CL/cl.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;
using namespace std;

#define MAX_SOURCE_SIZE (0x100000)

int main()
{
	/* VARIABLES SETUP */

	// ���� � �����������
	const char* imageFileName = "D:\\auto_contrast_processed.bmp";

	// �������� �����������
	CImg<unsigned char> image(imageFileName);

	// ���������� �������� �����������
	int H = image.height();
	int W = image.width();
	const int L = H * W;

	// ��������� �� ������ ��������� �����������
	unsigned char* h_grey_orig_image = image.data();

	// ���������� ������� ��� ��������������� �����-������ �����������
	unsigned char* h_grey_res_image = new unsigned char[L];

	/* ���������� ���������� */
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem d_grey_orig_image = NULL;
	cl_mem d_grey_res_image = NULL;
	cl_mem d_integral_image = NULL;
	cl_mem d_sum_image = NULL;
	cl_mem d_first_mean = NULL;
	cl_mem d_second_mean = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	size_t global_item_size;
	cl_int ret = NULL;
	cl_ulong t_start = 0,
		t_stop = 0,
		time_ex = 0,
		time_write = 0,
		time_read = 0,
		total_time = 0;
	char *source_str = (char*)malloc(MAX_SOURCE_SIZE);
	size_t source_size;

	LARGE_INTEGER
		freq,
		DoB_start,
		DoB_finish;
	QueryPerformanceFrequency(&freq);

	/* DEVICE SETUP */

	// ���������� ����������
	device_id = clGetDeviceId();

	// �������� OpenCL ���������
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// �������� ������� ������
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	// �������� ���� kernel
	ret = loadKernelFile("./DoB.cl", &source_size, source_str);

	// �������� ��������� �� ���� ���������
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);

	// �������������� ���������
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// � ������ ������ ������� ��� �����������
	if (ret == CL_BUILD_PROGRAM_FAILURE) {
		// ���������� ������ ����
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// �������� ������ ��� ���
		char *log = (char *)malloc(log_size);

		// �������� ���
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// ������� ���
		printf("%s\n", log);
		system("pause");
		exit(1);
	}

	/* INTEGRAL IMAGE */
	cl_event ndrEvt_1, ndrEvt_2;
	QueryPerformanceCounter(&DoB_start);

	// ��������� ������ �� ���������� ��� ����������� � �����
	d_grey_orig_image = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_grey_res_image = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_integral_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(unsigned int), NULL, &ret);
	d_sum_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(unsigned int), NULL, &ret);
	d_first_mean = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(unsigned char), NULL, &ret);
	d_second_mean = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(unsigned char), NULL, &ret);

	cl_event wrtEvt_1;
	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_grey_orig_image, CL_TRUE, 0, L * sizeof(unsigned char), h_grey_orig_image, 0, NULL, &wrtEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	//�������� kernel
	kernel = clCreateKernel(program, "scanStep1", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_grey_orig_image);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_sum_image);
	ret = clSetKernelArg(kernel, 2, sizeof(int), &W);

	// ������ ���������� �������
	global_item_size = H;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	//�������� kernel
	kernel = clCreateKernel(program, "scanStep2", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_sum_image);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_integral_image);
	ret = clSetKernelArg(kernel, 2, sizeof(int), &H);
	ret = clSetKernelArg(kernel, 3, sizeof(int), &W);

	// ������ ���������� �������
	global_item_size = W;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_2);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	/* DIFFERENCE OF MEANS */
	cl_event ndrEvt_3;
	float num_mean_1 = 3.0;
	float num_mean_2 = 5.0;
	float step = 5.5;

	//�������� kernel
	kernel = clCreateKernel(program, "diffOfMeans", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_integral_image);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_grey_res_image);
	ret = clSetKernelArg(kernel, 2, sizeof(int), &W);
	ret = clSetKernelArg(kernel, 3, sizeof(float), &num_mean_1);
	ret = clSetKernelArg(kernel, 4, sizeof(float), &num_mean_2);
	ret = clSetKernelArg(kernel, 5, sizeof(float), &step);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_3);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	/* RESULT */

	// ������� ��� ������
	cl_event readEvt_1;

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_grey_res_image, CL_TRUE, 0, L * sizeof(unsigned char), h_grey_res_image, 0, NULL, &readEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);
	QueryPerformanceCounter(&DoB_finish);

	// ������� �������������� �����������
	CImg<unsigned char> processedImage(W, H, 1, 1, false);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			processedImage(x, y) = h_grey_res_image[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	processedImage.save("D:\\d_of_means_processed.bmp");

	/* TIME MEASUREMENT */
	ret = clGetEventProfilingInfo(readEvt_1, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);

	// ����� ����� ����������� ������ ���������, ������� � ������ ������ ������ �� ���������� ������ ������
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	printf("\nTotal time of program working = %0.3f ms\n", ((t_stop - t_start) / 1000000.0));

	float DoB_delay = (DoB_finish.QuadPart - DoB_start.QuadPart) * 1000.0 / freq.QuadPart;
	cout << "QPC total time of program working = " << DoB_delay << " ms" << endl;

	system("pause");
	return 0;
}