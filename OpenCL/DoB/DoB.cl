__kernel void scanStep1(
	__global unsigned char* orig_img,
	__global unsigned int* sum_img,
	__private int width)
{
	uint y = get_global_id(0);
	unsigned int color = 0;
	unsigned int tempVal = 0;
	for (int x = 0; x < width; x++)
	{
		color = orig_img[y * width + x];
		tempVal += color;
		sum_img[y * width + x] = tempVal;
	}
}

__kernel void scanStep2(
	__global unsigned int* sum_img,
	__global unsigned int* res_img,
	__private int height,
	__private int width)
{
	uint x = get_global_id(0);
	unsigned int tempVal = 0;
	unsigned int color = 0;
	for (int y = 0; y < height; y++)
	{
		color = sum_img[x + y * width];
		tempVal += color;
		res_img[x + y * width] = tempVal;
	}
}

__kernel void diffOfMeans(
	__global unsigned int* integral_img,
	__global unsigned char* res_img,
	__private int width,
	__private float num_mean_1,
	__private float num_mean_2,
	__private float step)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;
	int hs_1 = 1;
	int hs_2 = 2;

	float num1 = num_mean_1 * num_mean_1;
	float num2 = num_mean_2 * num_mean_2;

	uint sum = 0;

	if ((x - hs_1) > 0 && (y - hs_1) > 0) {
		sum = integral_img[(x + hs_1) + (y + hs_1) * width]
			+ integral_img[(x - hs_1 - 1) + (y - hs_1 - 1) * width]
			- integral_img[(x - hs_1 - 1) + (y + hs_1) * width]
			- integral_img[(x + hs_1) + (y - hs_1 - 1) * width];
	}
	else sum = integral_img[(x + hs_1) + (y + hs_1) * width];

	float first_mean = sum / num1;
	sum = 0;

	if ((x - hs_2) > 0 && (y - hs_2) > 0) {
		sum = integral_img[(x + hs_2) + (y + hs_2) * width]
			+ integral_img[(x - hs_2 - 1) + (y - hs_2 - 1) * width]
			- integral_img[(x - hs_2 - 1) + (y + hs_2) * width]
			- integral_img[(x + hs_2) + (y - hs_2 - 1) * width];
	}
	else sum = integral_img[(x + hs_2) + (y + hs_2) * width];

	float second_mean = sum / num2;

	float p = first_mean - second_mean;

	res_img[x + y * width] = p > step ? 255 : 0;
}