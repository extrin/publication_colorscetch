__kernel void gaussianBlur (
					__global unsigned char* grey_image, 
				    __private int W,
				    __private int H,
				    __global float* mask,
				    __private int maskSize,
				    __global unsigned char* grey_out_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (y >= maskSize / 2 && x >= maskSize / 2 && y < (H - maskSize / 2) && x < (W - maskSize / 2)) {
		//���������� �����
		float sum = 0.0;

		for(int a = -maskSize / 2; a < maskSize / 2 + 1; ++a) {
			for(int b = -maskSize / 2; b < maskSize / 2 + 1; ++b) {
				float M = mask[a + maskSize / 2 + (b + maskSize / 2) * maskSize];
				sum += grey_image[(y + b) * W + (x + a)] * M;
			}
		}

		//������� ���������� ������ � �������� �������
		grey_out_image[coord] = sum > 2.7 ? 255 : 0;
	}
	else {
		grey_out_image[coord] = grey_image[coord];
	}

}