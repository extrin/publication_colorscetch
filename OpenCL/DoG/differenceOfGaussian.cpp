#include "../../CImg.h"
#include "../clFunctions.h"
#include <CL/cl.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;																							   
using namespace std;

#define MAX_SOURCE_SIZE (0x100000)

int main()
{
	/* VARIABLES SETUP */

	// ���� � �����������
	const char* imageFileName = "D:\\auto_contrast_processed.bmp";

	// �������� �����������
	CImg<unsigned char> image(imageFileName);
    	
	// ���������� �������� �����������
	int H = image.height();
    int W = image.width();
	const int L = H * W;

	// ��������� �� ������ ��������� �����������
	unsigned char* h_grey_orig_image = image.data();

	// ���������� ������� ��� ��������������� �����-������ �����������
	unsigned char* h_grey_res_image = new unsigned char[L];

	/* ���������� ���������� */
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem d_grey_orig_image = NULL;
	cl_mem d_grey_res_image = NULL;
	cl_mem d_mask_res = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	size_t global_item_size;
	cl_int ret = NULL;
	cl_ulong t_start = 0, 
		t_stop = 0, 
		time_ex = 0,
		time_write = 0,
		time_read = 0,
		total_time = 0;
	char *source_str = (char*)malloc(MAX_SOURCE_SIZE);
	size_t source_size;
	int maskSize = 5;
	float sigma_less = 1.0;
	float sigma_more = 1.6;

	LARGE_INTEGER
		freq,
		DoG_start,
		DoG_finish;
	QueryPerformanceFrequency(&freq);

	/* DEVICE SETUP */

	// ���������� ����������
	device_id = clGetDeviceId();

	// �������� OpenCL ���������
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// �������� ������� ������
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	ret = loadKernelFile("./gaussianBlur.cl", &source_size, source_str);

	// �������� ��������� �� ���� ���������
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
	 
	// �������������� ���������
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// � ������ ������ ������� ��� �����������
	if (ret == CL_BUILD_PROGRAM_FAILURE) {
		// ���������� ������ ����
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// �������� ������ ��� ���
		char *log = (char *) malloc(log_size);

		// �������� ���
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// ������� ���
		printf("%s\n", log);
		system("pause");
		exit(1);
	}

	/* GAUSSIAN BLUR */
	QueryPerformanceFrequency(&DoG_start);

	// ��������� ������ �� ���������� ��� ����������� � �����
	d_grey_orig_image = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_grey_res_image = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_mask_res = clCreateBuffer(context, CL_MEM_READ_ONLY, maskSize * maskSize * sizeof(float), NULL, &ret);

	float * h_mask_less = new float[maskSize * maskSize];
	float * h_mask_more = new float[maskSize * maskSize];
	float* h_mask = new float[maskSize * maskSize];

	//�������� kernel
	kernel = clCreateKernel(program, "gaussianBlur", &ret);	

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_grey_orig_image);
	ret = clSetKernelArg(kernel, 1, sizeof(int), &W);
	ret = clSetKernelArg(kernel, 2, sizeof(int), &H);
	ret = clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_mask_res);
	ret = clSetKernelArg(kernel, 4, sizeof(int), &maskSize);
	ret = clSetKernelArg(kernel, 5, sizeof(cl_mem), &d_grey_res_image);

	// ������� ��� �������� ������
	cl_event wrtEvt_1, wrtEvt_2;

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_grey_orig_image, CL_TRUE, 0, L * sizeof(unsigned char), h_grey_orig_image, 0, NULL, &wrtEvt_1);
	ret = clEnqueueWriteBuffer(command_queue, d_mask_res, CL_TRUE, 0, maskSize * maskSize * sizeof(float), h_mask, 0, NULL, &wrtEvt_2);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);	

	/* SUBTRACTION */
	createBlurMask(h_mask_less, sigma_less, maskSize);
	createBlurMask(h_mask_more, sigma_more, maskSize);
	for (int i = 0; i < maskSize * maskSize; ++i) {
		h_mask[i] = h_mask_less[i] - h_mask_more[i];
	}

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	cl_event ndrEvt_1;
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_1);
	
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	// ������� ��� ������
	cl_event readEvt_1;

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_grey_res_image, CL_TRUE, 0, L * sizeof(unsigned char), h_grey_res_image, 0, NULL, &readEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	
	oclFinish(&command_queue);
	QueryPerformanceFrequency(&DoG_finish);

	/* RESULT */

	// ������� �������������� �����������
	CImg<unsigned char> processedImage(W, H, 1, 1, false);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			processedImage(x, y) = h_grey_res_image[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	processedImage.save("D:\\difference_of_Gaussians_processed.bmp");

	/* ������� */
	ret = clReleaseMemObject(d_grey_orig_image);
	ret = clReleaseMemObject(d_grey_res_image);
	ret = clReleaseMemObject(d_mask_res);
	ret = clReleaseProgram(program);
	ret = clReleaseKernel(kernel);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context); 
	//delete[] h_grey_orig_image;
	//delete[] h_grey_res_image;
	//delete[] h_mask;
	//delete[] h_mask_less;
	//delete[] h_mask_more;

	/* TIME MEASUREMENT */
	// ����� ����� ����������� ������ ���������, ������� � ������ ������ ������ �� ���������� ������ ������
	ret = clGetEventProfilingInfo(readEvt_1, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	printf("\nTotal time of program working = %0.3f ms\n", ((t_stop - t_start) / 1000000.0));
	float DoG_delay = (DoG_finish.QuadPart - DoG_start.QuadPart) * 1000.0f / freq.QuadPart;
	cout << "QPC total time of program working = " << DoG_delay << " ms" << endl;

	system("pause");
	return 0;
}