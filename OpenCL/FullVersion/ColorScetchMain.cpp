#pragma once

#include "clFunctions.h"
#include <CL/cl.h>
#include "../../CImg.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;
using namespace std;

#define MAX_SOURCE_SIZE (0x100000)
#define HT 4000
#define CT 4000
#define lT 4000

int main()
{
	// ���� � �����������
	const char* imageFileName = "D:\\bear3.bmp";

	// �������� �����������
	CImg<cl_uchar> image(imageFileName);

	// ���������� �������� �����������
	cl_uint H = image.height();
	cl_uint W = image.width();
	cl_uint bW = W / 2;
	cl_uint bH = H / 2;
	const cl_uint L = H * W;
	const cl_uint bL = bH * bW;

	// ���������� ��������, ���������� RGB-���������� �������� �����������
	cl_uchar* h_orig_image_r = new cl_uchar[L];
	cl_uchar* h_orig_image_g = new cl_uchar[L];
	cl_uchar* h_orig_image_b = new cl_uchar[L];
	for (cl_int y = 0; y < H; y++)
	{
		for (cl_int x = 0; x < W; x++)
		{
			h_orig_image_r[y * W + x] = image(x, y, 0);
			h_orig_image_g[y * W + x] = image(x, y, 1);
			h_orig_image_b[y * W + x] = image(x, y, 2);
		}
	}

	// ���������� ������� ��� ��������������� �����������
	cl_uchar* h_res_image = new cl_uchar[L];

	/* ���������� ���������� */
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem d_orig_image_r = NULL,
		d_orig_image_g = NULL,
		d_orig_image_b = NULL,
		d_grey_image = NULL,
		d_grey_LUT = NULL,
		d_grey_histogram = NULL,
		d_grey_contrasted = NULL,
		d_integral_image = NULL,
		d_sum_image = NULL,
		d_first_mean = NULL,
		d_second_mean = NULL,
		d_grey_DoM_processed = NULL,
		d_temp_image = NULL,
		d_grey_closed = NULL,
		d_bLabels = NULL,
		d_labels = NULL,
		d_bConn = NULL,
		d_isChanged = NULL,
		d_block_hist = NULL,
		d_grey_suppressed = NULL,
		d_res_r = NULL,
		d_res_g = NULL,
		d_res_b = NULL;
	cl_program program = NULL;
	cl_kernel RGBToGreyscaleKernel = NULL,
		HistogramKernel = NULL,
		AutoContrastKernel = NULL,
		DoMKernelScanStep1 = NULL,
		DoMKernelScanStep2 = NULL,
		DoMKernelFinal = NULL,
		DilationKernel = NULL,
		ErosiaKernel = NULL,
		BlockMapInitKernel = NULL,
		ScanningKernel = NULL,
		AnalysisKernel = NULL,
		BlockHistKernel = NULL,
		FinalLabelsKernel = NULL,
		SuppressionKernel = NULL,
		BlendKernel = NULL;
	size_t global_item_size;
	cl_int ret = NULL;
	cl_ulong t_start = 0,
		t_stop = 0,
		time_ex = 0,
		time_write = 0,
		time_read = 0,
		total_time = 0;
	char *source_str = (char*)malloc(MAX_SOURCE_SIZE);
	size_t source_size;

	// ����������� � ������� �������������
	cl_uint *h_grey_histogram = new cl_uint[256], lSum = 0;
	memset(h_grey_histogram, 0, 256 * sizeof(cl_uint));
	cl_uchar *h_grey_LUT = new cl_uchar[256];
	cl_int i, LeftLim, RightLim;

	LARGE_INTEGER 
		freq, 
		RGBtG_start,
		RGBtG_finish,
		AC_start, 
		AC_finish,
		DoB_start,
		DoB_finish,
		Close_start,
		Close_finish,
		Sup_start,
		Sup_finish,
		Blend_start,
		Blend_finish,
		Common_start,
		Common_finish;
	QueryPerformanceFrequency(&freq);

	// ���������� ����������
	device_id = clGetDeviceId();

	// �������� OpenCL ���������
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// �������� ���� kernel
	ret = loadKernelFile("./ColorScetchKernels.cl", &source_size, source_str);

	// �������� ��������� �� ���� ���������
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);

	// �������������� ���������
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// � ������ ������ ������� ��� �����������
	if (ret != CL_SUCCESS) {
		// ���������� ������ ����
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// �������� ������ ��� ���
		char *log = (char *)malloc(log_size);

		// �������� ���
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// ������� ���
		printf("%s\n", log);
		system("pause");
		exit(1);
	}

	// �������� ������� ������
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	// ��������� ������ �� ����������
	d_orig_image_r = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_orig_image_g = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_orig_image_b = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_grey_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uchar), NULL, &ret);
	d_grey_histogram = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 256 * sizeof(cl_uint), NULL, &ret);
	d_grey_LUT = clCreateBuffer(context, CL_MEM_READ_ONLY, 256 * sizeof(cl_uchar), NULL, &ret);
	d_grey_contrasted = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uchar), NULL, &ret);
	d_integral_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uint), NULL, &ret);
	d_sum_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uint), NULL, &ret);
	d_first_mean = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uchar), NULL, &ret);
	d_second_mean = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uchar), NULL, &ret);
	d_grey_DoM_processed = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_bool), NULL, &ret);
	d_temp_image = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_bool), NULL, &ret);
	d_grey_closed = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_bool), NULL, &ret);
	d_bLabels = clCreateBuffer(context, CL_MEM_READ_WRITE, bL * sizeof(cl_uint), NULL, &ret);
	d_bConn = clCreateBuffer(context, CL_MEM_READ_WRITE, bL * sizeof(cl_uchar), NULL, &ret);
	d_isChanged = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint), NULL, &ret);
	d_block_hist = clCreateBuffer(context, CL_MEM_READ_WRITE, (bL + 1) * sizeof(cl_uint), NULL, &ret);
	d_labels = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uint), NULL, &ret);
	d_grey_suppressed = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_bool), NULL, &ret);
	d_res_r = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_res_g = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_res_b = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	// ������� ��� �������� ������
	cl_event wrtEvt;

	QueryPerformanceCounter(&Common_start);

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_r, CL_TRUE, 0, L * sizeof(cl_uchar), h_orig_image_r, 0, NULL, &wrtEvt);
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_g, CL_TRUE, 0, L * sizeof(cl_uchar), h_orig_image_g, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_b, CL_TRUE, 0, L * sizeof(cl_uchar), h_orig_image_b, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* RGB TO GREYSCALE */
	QueryPerformanceCounter(&RGBtG_start);
	//�������� kernel
	RGBToGreyscaleKernel = clCreateKernel(program, "RGBToGreyScale", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(RGBToGreyscaleKernel, 0, sizeof(cl_mem), &d_orig_image_r);
	ret = clSetKernelArg(RGBToGreyscaleKernel, 1, sizeof(cl_mem), &d_orig_image_g);
	ret = clSetKernelArg(RGBToGreyscaleKernel, 2, sizeof(cl_mem), &d_orig_image_b);
	ret = clSetKernelArg(RGBToGreyscaleKernel, 3, sizeof(cl_int), &L);
	ret = clSetKernelArg(RGBToGreyscaleKernel, 4, sizeof(cl_mem), &d_grey_image);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, RGBToGreyscaleKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&RGBtG_finish);
	/* AUTO CONTRAST */

	/* HISTOGRAM */
	QueryPerformanceCounter(&AC_start);
	//�������� kernel
	HistogramKernel = clCreateKernel(program, "histogram", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(HistogramKernel, 0, sizeof(cl_mem), &d_grey_image);
	ret = clSetKernelArg(HistogramKernel, 1, sizeof(cl_mem), &d_grey_histogram);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	cl_event ndrEvt_1 = NULL;
	ret = clEnqueueNDRangeKernel(command_queue, HistogramKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_grey_histogram, CL_TRUE, 0, 256 * sizeof(cl_uint), h_grey_histogram, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* CONTRAST */

	// ���������� ����� � ������ ������ ����������������

	// ���������� ����� ������� ��� ����������������
	lSum = 0;
	for (LeftLim = 0; LeftLim < 100; LeftLim++) {
		// H0 = H1 = HT ��. ���. �� ���������� ������
		if (h_grey_histogram[LeftLim] > HT) break;
		lSum += h_grey_histogram[LeftLim];
		// �0 = �1 = �T ��. ���. �� ���������� ������
		if (lSum > CT) break;
	}

	// ���������� ������ ������� ��� ����������������
	lSum = 0;
	for (RightLim = 255; RightLim > 150; RightLim--) {
		if (h_grey_histogram[RightLim] > lT) break;
		lSum += h_grey_histogram[RightLim];
		if (lSum > lT) break;
	}

	// ���������� ������� �������������
	for (i = 0; i < LeftLim; i++) { h_grey_LUT[i] = (cl_uchar)0; }
	for (i = LeftLim; i < RightLim; i++) {
		h_grey_LUT[i] = (cl_uchar)(255 * (i - LeftLim) / (RightLim - LeftLim));
	}
	for (i = RightLim; i < 256; i++) { h_grey_LUT[i] = (cl_uchar)255; }

	//�������� kernel
	AutoContrastKernel = clCreateKernel(program, "autoContrast", &ret);

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_grey_LUT, CL_TRUE, 0, 256 * sizeof(cl_uchar), h_grey_LUT, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(AutoContrastKernel, 0, sizeof(cl_mem), &d_grey_image);
	ret = clSetKernelArg(AutoContrastKernel, 1, sizeof(cl_mem), &d_grey_LUT);
	ret = clSetKernelArg(AutoContrastKernel, 2, sizeof(cl_mem), &d_grey_contrasted);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, AutoContrastKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&AC_finish);
	/* DIFFERENCE OF MEANS */
	QueryPerformanceCounter(&DoB_start);
	/* SCAN STEP 1 */

	//�������� kernel
	DoMKernelScanStep1 = clCreateKernel(program, "scanStep1", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(DoMKernelScanStep1, 0, sizeof(cl_mem), &d_grey_contrasted);
	ret = clSetKernelArg(DoMKernelScanStep1, 1, sizeof(cl_mem), &d_sum_image);
	ret = clSetKernelArg(DoMKernelScanStep1, 2, sizeof(cl_int), &W);

	// ������ ���������� �������
	global_item_size = H;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, DoMKernelScanStep1, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* SCAN STEP 2 */

	//�������� kernel
	DoMKernelScanStep2 = clCreateKernel(program, "scanStep2", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(DoMKernelScanStep2, 0, sizeof(cl_mem), &d_sum_image);
	ret = clSetKernelArg(DoMKernelScanStep2, 1, sizeof(cl_mem), &d_integral_image);
	ret = clSetKernelArg(DoMKernelScanStep2, 2, sizeof(cl_int), &H);
	ret = clSetKernelArg(DoMKernelScanStep2, 3, sizeof(cl_int), &W);

	// ������ ���������� �������
	global_item_size = W;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, DoMKernelScanStep2, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* DIFFERENCE OF MEANS FINAL */
	cl_float num_mean_1 = 3.0;
	cl_float num_mean_2 = 5.0;
	cl_float step = 5.5;

	//�������� kernel
	DoMKernelFinal = clCreateKernel(program, "diffOfMeans", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(DoMKernelFinal, 0, sizeof(cl_mem), &d_integral_image);
	ret = clSetKernelArg(DoMKernelFinal, 1, sizeof(cl_mem), &d_grey_DoM_processed);
	ret = clSetKernelArg(DoMKernelFinal, 2, sizeof(cl_int), &W);
	ret = clSetKernelArg(DoMKernelFinal, 3, sizeof(cl_float), &num_mean_1);
	ret = clSetKernelArg(DoMKernelFinal, 4, sizeof(cl_float), &num_mean_2);
	ret = clSetKernelArg(DoMKernelFinal, 5, sizeof(cl_float), &step);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, DoMKernelFinal, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&DoB_finish);
	/* CLOSE OPERATION */
	QueryPerformanceCounter(&Close_start);
	// ���������� ������� ��� ���������� �����������
	cl_bool* h_temp_image = new cl_bool[L];
	memset(h_temp_image, 0, L * sizeof(bool));
	ret = clEnqueueWriteBuffer(command_queue, d_temp_image, CL_TRUE, 0, L * sizeof(cl_bool), h_temp_image, 0, NULL, NULL);
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* DILATION */

	//�������� kernel
	DilationKernel = clCreateKernel(program, "dilation", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(DilationKernel, 0, sizeof(cl_mem), &d_grey_DoM_processed);
	ret = clSetKernelArg(DilationKernel, 1, sizeof(cl_int), &W);
	ret = clSetKernelArg(DilationKernel, 2, sizeof(cl_int), &H);
	ret = clSetKernelArg(DilationKernel, 3, sizeof(cl_mem), &d_temp_image);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, DilationKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* EROSIA */

	//�������� kernel
	ErosiaKernel = clCreateKernel(program, "erosia", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(ErosiaKernel, 0, sizeof(cl_mem), &d_temp_image);
	ret = clSetKernelArg(ErosiaKernel, 1, sizeof(int), &W);
	ret = clSetKernelArg(ErosiaKernel, 2, sizeof(int), &H);
	ret = clSetKernelArg(ErosiaKernel, 3, sizeof(cl_mem), &d_grey_closed);

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, ErosiaKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&Close_finish);
	/* SUPPRESSION OF SMALL AREAS */
	QueryPerformanceCounter(&Sup_start);
	//�������� kernel
	BlockMapInitKernel = clCreateKernel(program, "blockMapInit", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(BlockMapInitKernel, 0, sizeof(cl_mem), &d_grey_closed);
	ret = clSetKernelArg(BlockMapInitKernel, 1, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(BlockMapInitKernel, 2, sizeof(cl_mem), &d_bConn);
	ret = clSetKernelArg(BlockMapInitKernel, 3, sizeof(cl_uint), &bW);

	ScanningKernel = clCreateKernel(program, "scanningPhase", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(ScanningKernel, 0, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(ScanningKernel, 1, sizeof(cl_mem), &d_bConn);
	ret = clSetKernelArg(ScanningKernel, 2, sizeof(cl_uint), &bW);
	ret = clSetKernelArg(ScanningKernel, 3, sizeof(cl_mem), &d_isChanged);

	AnalysisKernel = clCreateKernel(program, "analysisPhase", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(AnalysisKernel, 0, sizeof(cl_mem), &d_bLabels);

	BlockHistKernel = clCreateKernel(program, "label_histogram", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(BlockHistKernel, 0, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(BlockHistKernel, 1, sizeof(cl_mem), &d_block_hist);

	FinalLabelsKernel = clCreateKernel(program, "SetFinalLabels", &ret);
	ret = clSetKernelArg(FinalLabelsKernel, 0, sizeof(cl_mem), &d_grey_closed);
	ret = clSetKernelArg(FinalLabelsKernel, 1, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(FinalLabelsKernel, 2, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(FinalLabelsKernel, 3, sizeof(cl_uint), &W);

	SuppressionKernel = clCreateKernel(program, "suppression", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(SuppressionKernel, 0, sizeof(cl_mem), &d_grey_suppressed);
	ret = clSetKernelArg(SuppressionKernel, 1, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(SuppressionKernel, 2, sizeof(cl_mem), &d_block_hist);
	ret = clSetKernelArg(SuppressionKernel, 3, sizeof(cl_uint), &W);

	/* LABELING STEP 1 */

	// ������ ���������� �������
	global_item_size = bL;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, BlockMapInitKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	cl_uint h_isChanged;
	cl_uint iterations = 0;

	do {

		if (iterations > 10) break;
		/* LABELLING STEP 2 */

		h_isChanged = 0;
		ret = clEnqueueWriteBuffer(command_queue, d_isChanged, CL_TRUE, 0, sizeof(cl_uint), &h_isChanged, 0, NULL, NULL);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		clFinish(command_queue);

		// ���������� kernel	
		ret = clEnqueueNDRangeKernel(command_queue, AnalysisKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		clFinish(command_queue);

		/* LABELLING STEP 3 */

		// ���������� kernel	
		ret = clEnqueueNDRangeKernel(command_queue, ScanningKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		clFinish(command_queue);

		ret = clEnqueueReadBuffer(command_queue, d_isChanged, CL_TRUE, 0, sizeof(cl_uint), &h_isChanged, 0, NULL, NULL);
		// �������� ��� ������� �� ���������� � ��������� �� ����������
		clFinish(command_queue);

		++iterations;
	} while (h_isChanged);

	/* SET FINAL LABELS */

	cl_uint* h_labels = new cl_uint[L];
	memset(h_labels, 0, L * sizeof(cl_uint));

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_labels, CL_TRUE, 0, L * sizeof(cl_uint), h_labels, 0, NULL, NULL);
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	global_item_size = L;
	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, FinalLabelsKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);


	/* HISTOGRAM OF LABELS */
	cl_uint* h_block_hist = new cl_uint[bL + 1];
	memset(h_block_hist, 0, sizeof(cl_uint)* (bL + 1));

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_block_hist, CL_TRUE, 0, (bL + 1) * sizeof(cl_uint), h_block_hist, 0, NULL, NULL);
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, BlockHistKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);

	/* SUPPRESSION */

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, SuppressionKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&Sup_finish);
	/* BLENDING */
	QueryPerformanceCounter(&Blend_start);
	//�������� kernel
	BlendKernel = clCreateKernel(program, "blending", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(BlendKernel, 0, sizeof(cl_mem), &d_grey_suppressed);
	ret = clSetKernelArg(BlendKernel, 1, sizeof(cl_mem), &d_orig_image_r);
	ret = clSetKernelArg(BlendKernel, 2, sizeof(cl_mem), &d_orig_image_g);
	ret = clSetKernelArg(BlendKernel, 3, sizeof(cl_mem), &d_orig_image_b);
	ret = clSetKernelArg(BlendKernel, 4, sizeof(cl_mem), &d_res_r);
	ret = clSetKernelArg(BlendKernel, 5, sizeof(cl_mem), &d_res_g);
	ret = clSetKernelArg(BlendKernel, 6, sizeof(cl_mem), &d_res_b);

	// ������ ���������� �������
	global_item_size = L;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, BlendKernel, 1, NULL, &global_item_size, NULL, 0, NULL, NULL);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&Blend_finish);
	/* RESULT */

	// ������� ��� ������
	cl_event readEvt;

	cl_uchar* h_res_r = new cl_uchar[L];
	cl_uchar* h_res_g = new cl_uchar[L];
	cl_uchar* h_res_b = new cl_uchar[L];

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_res_r, CL_TRUE, 0, L * sizeof(cl_uchar), h_res_r, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(command_queue, d_res_g, CL_TRUE, 0, L * sizeof(cl_uchar), h_res_g, 0, NULL, NULL);
	ret = clEnqueueReadBuffer(command_queue, d_res_b, CL_TRUE, 0, L * sizeof(cl_uchar), h_res_b, 0, NULL, &readEvt);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	clFinish(command_queue);
	QueryPerformanceCounter(&Common_finish);

	/* RELEASE CL OBJECTS */
	ret = clReleaseMemObject(d_orig_image_r);
	ret = clReleaseMemObject(d_orig_image_g);
	ret = clReleaseMemObject(d_orig_image_b);
	ret = clReleaseMemObject(d_grey_image);
	ret = clReleaseMemObject(d_grey_LUT);
	ret = clReleaseMemObject(d_grey_histogram);
	ret = clReleaseMemObject(d_grey_contrasted);
	ret = clReleaseMemObject(d_integral_image);
	ret = clReleaseMemObject(d_sum_image);
	ret = clReleaseMemObject(d_first_mean);
	ret = clReleaseMemObject(d_second_mean);
	ret = clReleaseMemObject(d_grey_DoM_processed);
	ret = clReleaseMemObject(d_temp_image);
	ret = clReleaseMemObject(d_grey_closed);
	ret = clReleaseMemObject(d_bLabels);
	ret = clReleaseMemObject(d_labels);
	ret = clReleaseMemObject(d_bConn);
	ret = clReleaseMemObject(d_isChanged);
	ret = clReleaseMemObject(d_block_hist);
	ret = clReleaseMemObject(d_grey_suppressed);
	ret = clReleaseMemObject(d_res_r);
	ret = clReleaseMemObject(d_res_g);
	ret = clReleaseMemObject(d_res_b);
	ret = clReleaseProgram(program);
	ret = clReleaseKernel(RGBToGreyscaleKernel);
	ret = clReleaseKernel(HistogramKernel);
	ret = clReleaseKernel(AutoContrastKernel);
	ret = clReleaseKernel(DoMKernelScanStep1);
	ret = clReleaseKernel(DoMKernelScanStep2);
	ret = clReleaseKernel(DoMKernelFinal);
	ret = clReleaseKernel(DilationKernel);
	ret = clReleaseKernel(ErosiaKernel);
	ret = clReleaseKernel(BlendKernel);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);

	// ������������� ������
	free(source_str);
	free(h_orig_image_r);
	free(h_orig_image_g);
	free(h_orig_image_b);
	free(h_res_image);
	free(h_grey_histogram);
	free(h_grey_LUT);
	free(h_labels);
	free(h_block_hist);

	/* TIME MEASUREMENT */

	float RGBtG_delay = (RGBtG_finish.QuadPart - RGBtG_start.QuadPart) * 1000.0f / freq.QuadPart;
	float AC_delay = (AC_finish.QuadPart - AC_start.QuadPart) * 1000.0f / freq.QuadPart;
	float DoB_delay = (DoB_finish.QuadPart - DoB_start.QuadPart) * 1000.0f / freq.QuadPart;
	float Close_delay = (Close_finish.QuadPart - Close_start.QuadPart) * 1000.0f / freq.QuadPart;
	float Sup_delay = (Sup_finish.QuadPart - Sup_start.QuadPart) * 1000.0f / freq.QuadPart;
	float Blend_delay = (Blend_finish.QuadPart - Blend_start.QuadPart) * 1000.0f / freq.QuadPart;
	float Common_delay = (Common_finish.QuadPart - Common_start.QuadPart) * 1000.0f / freq.QuadPart;

	cout << endl << "QPC RGB to Greyscale = " << RGBtG_delay << " ms.";
	cout << endl << "QPC Autocontrast = " << AC_delay << " ms.";
	cout << endl << "QPC Difference of Boxes = " << DoB_delay << " ms.";
	cout << endl << "QPC Close operation = " << Close_delay << " ms.";
	cout << endl << "QPC Suppression = " << Sup_delay << " ms.";
	cout << endl << "QPC Blending = " << Blend_delay << " ms.";
	cout << endl << "QPC Total time of program working = " << Common_delay << " ms.";

	// ����� ����������� ������ � ���������� � ������������
	ret = clGetEventProfilingInfo(readEvt, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &t_stop, NULL);

	// ����� ����� ����������� ������ ���������, ������� � ������ ������ ������ �� ���������� ������ ������
	ret = clGetEventProfilingInfo(wrtEvt, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &t_start, NULL);
	printf("\nOpenCL Total time of working on device = %0.3f ms\n", ((t_stop - t_start) / 1000000.0));

	// ������� �������������� �����������
	CImg<cl_uchar> processedImage(image);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			processedImage(x, y, 0) = h_res_r[x + y * W];
			processedImage(x, y, 1) = h_res_g[x + y * W];
			processedImage(x, y, 2) = h_res_b[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	processedImage.save("D:\\color_scetch_processed_norm.bmp");

	free(h_res_r);
	free(h_res_g);
	free(h_res_b);

	system("pause");
	return 0;
}