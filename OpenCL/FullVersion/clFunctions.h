#pragma once

#include <CL/cl.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

// ��������� ��������������� �������
int waitForEventAndRelease(cl_event *event);
void oclFinish(cl_command_queue *command_queue);
cl_device_id clGetDeviceId();
int loadKernelFile(const char* fileName, size_t* source_size, char* source_str);