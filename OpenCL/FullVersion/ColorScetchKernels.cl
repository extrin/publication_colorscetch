__kernel void RGBToGreyScale(
	__global uchar* r_image,
	__global uchar* g_image,
	__global uchar* b_image,
	__private int L,
	__global uchar* grey_out_image)
{
	int coord = get_global_id(0);
	float multR = 0.299,
		multG = 0.587,
		multB = 0.114;
	grey_out_image[coord] = r_image[coord] * multR + g_image[coord] * multG + b_image[coord] * multB;
}

__kernel void histogram(
	__global uchar* grey_image,
	__global uint* histogram)
{
	int coord = get_global_id(0);
	atomic_inc(&histogram[grey_image[coord]]);
}

__kernel void autoContrast(
	__global uchar* grey_orig_image,
	__global uchar* LUT,
	__global uchar* grey_res_image)
{
	int coord = get_global_id(0);
	grey_res_image[coord] = LUT[grey_orig_image[coord]];
}

__kernel void scanStep1(
	__global uchar* orig_img,
	__global uint* sum_img,
	__private int width)
{
	uint y = get_global_id(0);
	uint color = 0;
	uint tempVal = 0;
	for (int x = 0; x < width; x++)
	{
		color = orig_img[y * width + x];
		tempVal += color;
		sum_img[y * width + x] = tempVal;
	}
}

__kernel void scanStep2(
	__global uint* sum_img,
	__global uint* res_img,
	__private int height,
	__private int width)
{
	uint x = get_global_id(0);
	uint tempVal = 0;
	uint color = 0;
	for (int y = 0; y < height; y++)
	{
		color = sum_img[x + y * width];
		tempVal += color;
		res_img[x + y * width] = tempVal;
	}
}

__kernel void diffOfMeans(
	__global uint* integral_img,
	__global bool* res_img,
	__private int width,
	__private float num_mean_1,
	__private float num_mean_2,
	__private float step)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;
	int hs_1 = 1;
	int hs_2 = 2;

	float num1 = num_mean_1 * num_mean_1;
	float num2 = num_mean_2 * num_mean_2;

	uint sum = 0;

	if ((x - hs_1) > 0 && (y - hs_1) > 0) {
		sum = integral_img[(x + hs_1) + (y + hs_1) * width]
			+ integral_img[(x - hs_1 - 1) + (y - hs_1 - 1) * width]
			- integral_img[(x - hs_1 - 1) + (y + hs_1) * width]
			- integral_img[(x + hs_1) + (y - hs_1 - 1) * width];
	}
	else sum = integral_img[(x + hs_1) + (y + hs_1) * width];

	float first_mean = sum / num1;
	sum = 0;

	if ((x - hs_2) > 0 && (y - hs_2) > 0) {
		sum = integral_img[(x + hs_2) + (y + hs_2) * width]
			+ integral_img[(x - hs_2 - 1) + (y - hs_2 - 1) * width]
			- integral_img[(x - hs_2 - 1) + (y + hs_2) * width]
			- integral_img[(x + hs_2) + (y - hs_2 - 1) * width];
	}
	else sum = integral_img[(x + hs_2) + (y + hs_2) * width];

	float second_mean = sum / num2;

	float p = first_mean - second_mean;

	res_img[x + y * width] = p > step ? true : false;
}

__kernel void dilation(
	__global bool* grey_image,
	__private int W,
	__private int H,
	__global bool* temp_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (grey_image[coord]) {
			temp_image[(y - 1) * W + (x - 1)] = true;
			temp_image[(y - 1) * W + x] = true;
			temp_image[(y - 1) * W + (x + 1)] = true;
			temp_image[y * W + (x - 1)] = true;
			temp_image[y * W + x] = true;
			temp_image[y * W + (x + 1)] = true;
			temp_image[(y + 1) * W + (x - 1)] = true;
			temp_image[(y + 1) * W + x] = true;
			temp_image[(y + 1) * W + (x + 1)] = true;
		}

	}
}

__kernel void erosia(
	__global bool* temp_image,
	__private int W,
	__private int H,
	__global bool* grey_out_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;
	bool pixel = temp_image[coord];

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		grey_out_image[coord] = pixel;

		bool temp;
		if (pixel) {
			temp = min(temp_image[(y - 1) * W + (x - 1)],
				min(temp_image[(y - 1) * W + x],
				min(temp_image[(y - 1) * W + (x + 1)],
				min(temp_image[y * W + (x - 1)],
				min(temp_image[y * W + x],
				min(temp_image[y * W + (x + 1)],
				min(temp_image[(y + 1) * W + (x - 1)],
				min(temp_image[(y + 1) * W + x], temp_image[(y + 1) * W + (x + 1)]))))))));
			grey_out_image[coord] = temp;
		}
	}
}

__kernel void blending(
	__global bool* mask,
	__global uchar* orig_r,
	__global uchar* orig_g,
	__global uchar* orig_b,
	__global uchar* res_r,
	__global uchar* res_g,
	__global uchar* res_b)
{
	uint coord = get_global_id(0);

	res_r[coord] = 0;
	res_g[coord] = 0;
	res_b[coord] = 0;
	if (mask[coord]) {
		res_r[coord] = orig_r[coord];
		res_g[coord] = orig_g[coord];
		res_b[coord] = orig_b[coord];
	}
}

bool HasBit(uint pattern, uint position)
{
	return pattern & (1 << position);
}

long FindMinLabel(__global uchar* bConn, __global uint* bLabels, uint Pos, uint width)
{
	uint x = Pos % width;
	uint y = Pos / width;
	uint height = get_global_size(0) / width;

	uint labels[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

	if (x > 0 && y > 0 && x < width - 1 && y < height - 1) {
		labels[0] = bLabels[x - 1 + (y - 1)*width];
		labels[1] = bLabels[x + (y - 1)*width];
		labels[2] = bLabels[x + 1 + (y - 1)*width];
		labels[3] = bLabels[x - 1 + y*width];
		labels[4] = bLabels[x + 1 + y*width];
		labels[5] = bLabels[x - 1 + (y + 1)*width];
		labels[6] = bLabels[x + (y + 1)*width];
		labels[7] = bLabels[x + 1 + (y + 1)*width];
	}

	uint L = bLabels[Pos];

	uchar bConnEl = bConn[Pos];

	uint minL = 2147483647;

	for (uint i = 0; i < 8; ++i)
	{
		if (HasBit(bConnEl, i) && labels[i] < minL) minL = labels[i];
	}

	return minL;
}


__kernel void blockMapInit(
	__global bool* orig_img,
	__global uint* bLabels,
	__global uchar* bConn,
	__private uint width)
{
	uint coord = get_global_id(0);
	uint size = get_global_size(0);
	uint height = size / width;
	uint x = coord % width;
	uint y = coord / width;
	uint imgWidth = width * 2;
	uchar bConnItem = 0x0;
	uint bLabelsItem = 0;

	uint P = 0x0;
	uint P0 = 0x777;

	if (orig_img[2 * x + 2 * y*imgWidth]) P = P | P0;
	if (orig_img[(2 * x + 1) + 2 * y*imgWidth]) P = P | (P0 << 1);
	if (orig_img[2 * x + (2 * y + 1)*imgWidth]) P = P | (P0 << 4);
	if (orig_img[(2 * x + 1) + (2 * y + 1)*imgWidth]) P = P | (P0 << 5);

	if (P > 0) {
		bLabelsItem = coord + 1;
		if (x > 0 && y > 0 && x < (width - 1) && y < (height - 1)) {
			if (HasBit(P, 0x0) && orig_img[(2 * x - 1) + (2 * y - 1)*imgWidth])
				bConnItem |= 1 << 0x0;
			if ((HasBit(P, 0x1) && orig_img[2 * x + (2 * y - 1)*imgWidth]) || (HasBit(P, 0x2) && orig_img[(2 * x + 1) + (2 * y - 1)*imgWidth]))
				bConnItem |= 1 << 0x1;
			if (HasBit(P, 0x3) && orig_img[(2 * x + 2) + (2 * y - 1)*imgWidth])
				bConnItem |= 1 << 0x2;
			if ((HasBit(P, 0x4) && orig_img[(2 * x - 1) + 2 * y*imgWidth]) || (HasBit(P, 0x8) && orig_img[(2 * x - 1) + (2 * y + 1)*imgWidth]))
				bConnItem |= 1 << 0x3;
			if ((HasBit(P, 0x7) && orig_img[(2 * x + 2) + 2 * y*imgWidth]) || (HasBit(P, 0xB) && orig_img[(2 * x + 2) + (2 * y + 1)*imgWidth]))
				bConnItem |= 1 << 0x4;
			if (HasBit(P, 0xC) && orig_img[(2 * x - 1) + (2 * y + 2)*imgWidth])
				bConnItem |= 1 << 0x5;
			if ((HasBit(P, 0xD) && orig_img[2 * x + (2 * y + 2)*imgWidth]) || (HasBit(P, 0xE) && orig_img[(2 * x + 1) + (2 * y + 2)*imgWidth]))
				bConnItem |= 1 << 0x6;
			if (HasBit(P, 0xF) && orig_img[(2 * x + 2) + (2 * y + 2)*imgWidth])
				bConnItem |= 1 << 0x7;
		}
	}
	else bLabelsItem = 0;

	bConn[coord] = bConnItem;
	bLabels[coord] = bLabelsItem;
}

__kernel void scanningPhase(
	__global uint* bLabels,
	__global uchar* bConn,
	__private uint width,
	__global uint* isChanged)
{
	uint coord = get_global_id(0);
	uint L = bLabels[coord];

	if (L)
	{
		uint Lmin = FindMinLabel(bConn, bLabels, coord, width);
		if (Lmin < L)
		{
			uint L1 = bLabels[L - 1];
			if (Lmin < L1) {
				bLabels[L - 1] = Lmin;
				atomic_inc(isChanged);
			}
		}
	}
}

__kernel void analysisPhase(__global uint* bLabels)
{
	uint coord = get_global_id(0);
	uint L = bLabels[coord];

	if (L > 0)
	{
		uint Lcur = bLabels[L - 1];
		while (Lcur != L)
		{
			L = bLabels[Lcur - 1];
			Lcur = bLabels[L - 1];
		}
		bLabels[coord] = L;
	}
}

__kernel void SetFinalLabels(
	__global bool* orig_img,
	__global uint* bLabels,
	__global uint* labels,
	__private uint width)
{
	uint coord = get_global_id(0);
	uint x = coord % width;
	uint y = coord / width;
	const size_t spos = (x >> 1) + (y >> 1) * ceil((float)width / 2);

	labels[coord] = orig_img[coord] ? bLabels[spos] : 0;
}

__kernel void label_histogram(
	__global uint* labels,
	__global uint* histogram)
{
	int coord = get_global_id(0);
	atomic_inc(&histogram[labels[coord]]);
}

__kernel void suppression(
	__global bool* res_image,
	__global uint* labels,
	__global uint* histogram,
	__private uint width)
{
	uint coord = get_global_id(0);

	if (labels[coord] && histogram[labels[coord]] > 20)
		res_image[coord] = true;
	else
		res_image[coord] = false;
}