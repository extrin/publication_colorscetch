
class Stack   //��� ��������� FloodFill
{
    int X,Y;
    Stack *Next;
public:
	int GetX () const;
    int GetY () const;
	friend Stack* Push(Stack*,int,int);
	friend Stack* Pop(Stack*);
};

//-----------------------------------------------------------------------------------------
//                             MAIN
//-----------------------------------------------------------------------------------------
		//�������� ������� �������� � ��������� ��������
		//int** small_S=new int*[L];//������ ��������� ����� ��� �������� ����� �������
		//for (int i=0;i<L;i++)
		//{
		//	small_S[i]=new int[2];
		//}
		//int num_S=0;//������� ��� ����� ��������
		//for (int y=0;y<H;y++)//��� ���� �������� �����������
		//	for (int x=0;x<W;x++) 
		//		if (!(Ind[y*W+x]) && grayscale_image[y*W+x]!=0)//��������� �����
		//		{
		//			int S=FloodFill(grayscale_image,Ind,x,y,H,W,false);//������� ������� ��������� � (�,�) ������� 
		//			if (S<AREA)
		//			{
		//				small_S[num_S][0]=x;//���������� ���������� ��������� �����
		//				small_S[num_S][1]=y;
		//				num_S++; 
		//			}
		//		}
		//memset(Ind,false,L);//��������� ������ Ind ���������� false
		//for (int i=0;i<num_S;i++)
		//	int S=FloodFill(grayscale_image,Ind,small_S[i][0],small_S[i][1],H,W,true);//S �� �����, ����������� �������
		


//-----------------------------------------------------------------------------------------
//                             ������� ��������
//-----------------------------------------------------------------------------------------
int FloodFill(unsigned char* Matrix,bool* Ind,int XStart,int YStart,const int H,const int W,bool fill)//���������� �������
{
	int S=0;//��� ����������� �������
	Stack *Head=Push(nullptr,XStart,YStart); //������� ���� 
	while (Head)
	{
		int X=Head->GetX();
		int y=Head->GetY();
		Ind[y*W+X]=true;//��������, ��� �� ����� ����
		S++;//����������� �������
		if (fill)//���� ����� ������ �����
			Matrix[y*W+X]=0;
		Head=Pop(Head);

		int xl=X-1;
		int xr=X+1;

		if (Ind[y*W+xl])
			continue;   

		//��������� � ����� �����
		while( xl >= 0 && (Matrix[y*W+xl]!=0 && !(Ind[y*W+xl])))//�� ������� � �� ���� ����������������
		{
			Ind[y*W+xl] = true;
			S++;
			if (fill)//���� ����� ������ �����
				Matrix[y*W+xl]=0;
			xl--;
		}
            ++xl;   //������������ � ���������� �������

            //��������� � ������ �����
            while( xr < W && Matrix[y*W+xr]!=0 && !(Ind[y*W+xr]) )
			{
				Ind[y*W+xr] = true;
				S++;
				if (fill)//���� ����� ������ �����
					Matrix[y*W+xr]=0;
				xr++;
			}
            --xr;

			//��������� ������� ����� (���� ��� ����������)
            bool bLeftBack = true;
            if( y > 0 )
                for( int x = xl; x <= xr; ++x )
                {
                    if(Matrix[(y-1)*W+x]!=0 && !(Ind[(y-1)*W+x]))
                    {
                        if( bLeftBack )
                            Head=Push(Head,x,y-1);
                        bLeftBack = false; 
                    }
                    else
                        bLeftBack = true;
                }
				 //��������� ������ ����� (���� ��� ����������)
            bLeftBack = true;
            if (y < H )
                for( int x = xl; x <= xr; ++x )
                {
                    // check for being out of zone
                    if (Matrix[(y+1)*W+x]!=0 && !(Ind[(y+1)*W+x]))
                    {
                        if( bLeftBack )
                            Head=Push(Head,x,y+1);
                        bLeftBack = false;
                    }
                    else
                        bLeftBack = true;
                }
	}
	return S;
}
//-----------------------------------------------------------------------------------------
//                             ���������� �������� � ����
//-----------------------------------------------------------------------------------------
Stack *Push(Stack *head,int Xnew,int Ynew)    
{
	Stack *NewElement=new Stack;
	NewElement->Next=head;
	NewElement->X=Xnew;
	NewElement->Y=Ynew;
	return NewElement;
}
//-----------------------------------------------------------------------------------------
//                             ������� �������� �� �����
//-----------------------------------------------------------------------------------------
Stack *Pop(Stack *head) 
{
	if (head)
	{
	Stack *pElement=head->Next;
    delete head;
	return pElement;
	}
	return nullptr;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetX
//-----------------------------------------------------------------------------------------
int Stack::GetX () const
{
	return X;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetY
//-----------------------------------------------------------------------------------------
int Stack::GetY () const
{
	return Y;
}
