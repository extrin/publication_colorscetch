using namespace std;
#define _CRT_SECURE_NO_WARNINGS
#pragma pack(push,1)
#include <iostream>
#include "bmp.h"
#include <stdio.h>
#include <math.h>
#include <io.h>
#include <random>
#include <emmintrin.h>
#include <time.h> 
#include <Windows.h>

LARGE_INTEGER frequency, start, finish;
#define NUM_THREADS 8

const unsigned char umask1[16]={0,2,4,6,8,10,12,14,1,3,5,7,9,11,13,15};		//����� ��� ������������ u16b �� ������� ����� 0-7
const unsigned char umask2[16]={1,3,5,7,9,11,13,15,0,2,4,6,8,10,12,14};		//����� ��� ������������ u16b �� ������� ����� 8-15
const __m128i mask1=_mm_loadu_si128((__m128i*)(umask1));					//��������� �������� ����� � ������ __m128i
const __m128i mask2=_mm_loadu_si128((__m128i*)(umask2));

class BMPImage
{
	bmpheader		BMPhead;
	bmpinfo			BMPinf;
	COLOR*			Blue;
	COLOR*			Green;
	COLOR*			Red;
	BGR*			palitre;
	char*			InputPath;
	unsigned int	count;
	unsigned char	pad;
public:
	COLOR*			Gray;	
	unsigned int*	IntImage;
	BMPImage(char* Path1);
	void BMPWrite(char* num);
	void GetGray();
	void GetGray_simd();
	void GlobalContrastCorrection();
	void IntegralImage();
	void IntegralImage_simd();
	void DoG();
	void DoG_simd();
	void Closing();
	void Closing_simd();
	void Filling();
	void Filling_simd();
	void Blending();
	void Blending_simd();
};

BMPImage::BMPImage(char* Path1)
{
	InputPath  = Path1;
	int fin = _open(InputPath, _OREAD | _OBINARY);
	_read(fin, &BMPhead, sizeof(bmpheader));
	_read(fin, &BMPinf, sizeof(bmpinfo));
	pad=0;
	count=BMPinf.Width*BMPinf.Height;
	if (BMPinf.BitCount==24) 
	{
		if ((BMPinf.Width * 3) % 4 != 0) pad = 4 - ((BMPinf.Width * 3) % 4);
		palitre=new BGR [BMPinf.ClrUsed];
		for (int i=0;i<BMPinf.ClrUsed;++i)
			_read(fin,&palitre[i],sizeof(BGR));

		Blue		  = (COLOR*)_mm_malloc(count,16);
		Green		  = (COLOR*)_mm_malloc(count,16);
		Red			  = (COLOR*)_mm_malloc(count,16);
		Gray		  = (COLOR*)_mm_malloc(count,16);
		IntImage	  = new unsigned[count];
		unsigned long buf_padding;
		for(unsigned int x=0;x<count;++x)
		{
			_read(fin,&Blue [x],sizeof(COLOR));
			_read(fin,&Green[x],sizeof(COLOR));
			_read(fin,&Red  [x],sizeof(COLOR));
			if(( (x+1) % BMPinf.Width == 0) && ( pad != 0 )) _read(fin,&buf_padding,pad);
		}
	}
	else if (BMPinf.BitCount==8)
	{
		palitre=new BGR [BMPinf.ClrUsed];
		for (int i=0;i<BMPinf.ClrUsed;++i)
			_read(fin,&palitre[i],sizeof(COLOR));
		count=BMPinf.Width*BMPinf.Height;
		for(unsigned int x=0;x<count;++x)
			_read(fin,&Gray[x],sizeof(COLOR));
	}
	_close(fin);
	cout<<"������������� ����� "<< InputPath << " ���������"<<endl;
}

void BMPImage::BMPWrite(char* num)
{
	unsigned char padding_value=0;
	char OutputPath[200];
	strcpy(OutputPath, InputPath);
	OutputPath[strlen(OutputPath)-4]='\0';
	strcat(OutputPath, "_out");
	strcat(OutputPath,num);
	strcat(OutputPath, ".bmp");
	int fout = _open(OutputPath, _OCREATE| _OWRITE | _OBINARY);
	_write(fout,&BMPhead,sizeof(BMPhead));
	_write(fout,&BMPinf,sizeof(BMPinf));
	for(int i=0;i<BMPinf.ClrUsed;++i)
	{
		_write(fout,&palitre[i],sizeof(BGR));
	}
	if (BMPinf.BitCount==24) 
	{
		for(unsigned int x=0;x<count;++x)
		{
			/*_write(fout,&Gray[x],sizeof(COLOR));
			_write(fout,&Gray[x],sizeof(COLOR));
			_write(fout,&Gray[x],sizeof(COLOR));*/
			_write(fout,&Blue[x],sizeof(COLOR));
			_write(fout,&Green[x],sizeof(COLOR));
			_write(fout,&Red[x],sizeof(COLOR));
			if(( (x+1) % BMPinf.Width == 0) && ( pad != 0 )) for (int p=0;p<pad;++p) _write(fout,&padding_value,sizeof(padding_value));
		}
	}
	_close(fout);
	cout<<"������ ���������  �� ������: "<< OutputPath <<endl;
}

void BMPImage::GlobalContrastCorrection()
{
	int HT=3000,CT=1000;
	int Hist[256];
	int left,right;
	for(int y=0;y<256;++y) Hist[y]=0;
	for(int y=0;y<count;++y) ++Hist[Gray[y]];
	int lsum=0,rsum=0;
	for ( left=0;left<100;++left)
	{
		if (Hist[left] >HT) break;
		lsum+=Hist[left];
		if (lsum > CT) break;
	}
	for (right=255;right>150;--right)
	{
		if (Hist[right]>HT) break;
		rsum+=Hist[right];
		if (rsum > CT) break;
	}
	float GCC[256];
	for (int i=0;i<left;++i)			GCC[i]=0;
	for (int i=left;i<right;++i)		GCC[i]=255*(i-left)/(right-left);
	for (int i=right;i<256;++i)			GCC[i]=255;
	for (unsigned int y=0;y<count;++y)  Gray[y]=GCC[Gray[y]];
cout<<"��������� ����������� ��������� ��������� "<<endl;		
}

inline __m128i two_8b_to_16b (__m128i first,__m128i second)
{
	first  = _mm_shuffle_epi8 (first, mask1);									
	second = _mm_shuffle_epi8 (second,mask2);									
	return   _mm_add_epi8 (first,second);
}

void BMPImage::GetGray()
{
	for (unsigned int x=0;x<count;++x)
	{
		Gray[x]=(Red[x]+2*Green[x]+Blue[x])/4;
	}
}

void BMPImage::GetGray_simd()
{
	__m128i zero=_mm_setzero_si128();
	__m128i red_buf,green_buf,blue_buf,buf;
	//#pragma omp parallel for num_threads(NUM_THREADS)
	for (int x=0;x<count;x+=sizeof(__m128i))
	{
		__m128i sum1= _mm_setzero_si128();
		__m128i sum2= _mm_setzero_si128();
		
		red_buf = _mm_loadu_si128((__m128i*)(Red+x));				//��������� 16 1b ��������
			 buf = _mm_cvtepu8_epi16(red_buf);					//������� ������ 8 ����� 1b � 2b
			 sum1=_mm_adds_epi16(sum1,buf);						//�������� ������ 8 2b �����
			 buf = _mm_unpackhi_epi8(red_buf, zero);			//������� ������ 8 ����� 1b � 2b
			 sum2=_mm_adds_epi16(sum2,buf);						//�������� ������ 8 2b �����	

		green_buf = _mm_loadu_si128((__m128i*)(Green+x));				
			 buf = _mm_cvtepu8_epi16(green_buf);					
			 sum1=_mm_adds_epi16(sum1,buf);	
			 sum1=_mm_adds_epi16(sum1,buf);
			 buf = _mm_unpackhi_epi8(green_buf, zero);			
			 sum2=_mm_adds_epi16(sum2,buf);
			 sum2=_mm_adds_epi16(sum2,buf);

		blue_buf = _mm_loadu_si128((__m128i*)(Blue+x));				
			 buf = _mm_cvtepu8_epi16(blue_buf);					
			 sum1=_mm_adds_epi16(sum1,buf);						
			 buf = _mm_unpackhi_epi8(blue_buf, zero);			
			 sum2=_mm_adds_epi16(sum2,buf);

		sum1=_mm_srai_epi16(sum1,2);										
		sum2=_mm_srai_epi16(sum2,2);
		_mm_storeu_si128((__m128i*)(Gray + x), two_8b_to_16b(sum1,sum2));						//�������� �������� �� ������
	}
}

void BMPImage::IntegralImage()
{
	IntImage[0]=Gray[0];
	for (int x=1;x<BMPinf.Width;++x)  
		IntImage[x] = Gray[x] + IntImage[x-1];
	for (int x=1;x<BMPinf.Height;++x) 
		IntImage[x*BMPinf.Width] = Gray[x*BMPinf.Width] + IntImage[(x-1)*BMPinf.Width];
	for (int y=1;y<BMPinf.Height;++y)
		for (int x=1;x<BMPinf.Width;++x)
			IntImage[x+y*BMPinf.Width] = Gray[x+y*BMPinf.Width]+IntImage[x+y*BMPinf.Width-1]+IntImage[x+(y-1)*BMPinf.Width]-IntImage[x+(y-1)*BMPinf.Width-1];
}

void BMPImage::IntegralImage_simd()
{
	__m64 mask=_mm_set_pi8 (7,5,3,1,6,4,2,0);
	for (int y=0;y<BMPinf.Height;++y)
		IntImage[y*BMPinf.Width]=Gray[y*BMPinf.Width];
	for (int y=0;y<BMPinf.Height;++y)
		for (int x=1;x<BMPinf.Width;++x)
		{
			IntImage[x+y*BMPinf.Width]=Gray[x+y*BMPinf.Width]+IntImage[x+y*BMPinf.Width-1];
		}
	for (int x=0;x<BMPinf.Width;x+=sizeof(__m128i)/4)
	{
		__m128i sum1=_mm_setzero_si128();
		for (int y=0;y<BMPinf.Height;++y)
		{
			__m128i buf = _mm_loadu_si128((__m128i*)(Gray+x+y*BMPinf.Width));
			
			sum1=_mm_add_epi32(sum1,_mm_loadu_si128((__m128i*)(Gray+x+y*BMPinf.Width)));
			_mm_storeu_si128((__m128i*)(IntImage + x + y*BMPinf.Width),sum1);
		}
	}
}

void BMPImage::DoG()//3x3 & 7x7
{
	float z1,z2;	
	int thr=5;
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;++x)
		{
		  //z1=IntImage[x+(y+3)*BMPinf.Width+3]-IntImage[x+(y+3)*BMPinf.Width-4]+IntImage[x+(y-4)*BMPinf.Width-4]-IntImage[x+(y-4)*BMPinf.Width+3];
			z1=IntImage[x+(y+2)*BMPinf.Width+2]-IntImage[x+(y+2)*BMPinf.Width-3]+IntImage[x+(y-3)*BMPinf.Width-3]-IntImage[x+(y-3)*BMPinf.Width+2];
			z2=IntImage[x+(y+1)*BMPinf.Width+1]-IntImage[x+(y+1)*BMPinf.Width-2]+IntImage[x+(y-2)*BMPinf.Width-2]-IntImage[x+(y-2)*BMPinf.Width+1];
			z1=z2/9.-z1/25.;
			if (z1<thr) z1=0;
			if (z1>=thr) z1=255;
			Gray[x+y*BMPinf.Width]=(int)(z1+0.5);
		}
}

void BMPImage::DoG_simd()//3x3 & 5x5 & 7x7
{
	__m128i sum1,sum2;
	__m64 dog;
	float div1=25.0f,div2=9.0f;
	__m64 ed=_mm_set1_pi16(1),treshold=_mm_set1_pi16(3),max= _mm_set1_pi16(255);
	__m64 mask=_mm_set_pi8 (7,5,3,1,6,4,2,0);
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;x+=4)
		{
			//sum1=_mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+3)*BMPinf.Width+3)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-4)*BMPinf.Width-4))),_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+3)*BMPinf.Width-4)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-4)*BMPinf.Width+3)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 7�7
			sum1=_mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+2)*BMPinf.Width+2)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-3)*BMPinf.Width-3))),_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+2)*BMPinf.Width-3)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-3)*BMPinf.Width+2)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 5�5
			sum2=_mm_sub_epi32(_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+1)*BMPinf.Width+1)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-2)*BMPinf.Width-2))),_mm_add_epi32(_mm_loadu_si128((__m128i*)(IntImage+x+(y+1)*BMPinf.Width-2)),_mm_loadu_si128((__m128i*)(IntImage+x+(y-2)*BMPinf.Width+1)))); //�������� 4 ����� + ������ ����� � �������� �� ����� 3�3
			dog=_mm_cvtps_pi16(_mm_sub_ps(_mm_div_ps(_mm_cvtepi32_ps(sum2),_mm_load1_ps(&div2)),_mm_div_ps(_mm_cvtepi32_ps(sum1),_mm_load1_ps(&div1))));//�������������� � �����-> ������� -> ��������� -> �������������� � __m64
			dog=_mm_cmpgt_pi16(treshold,dog);//c��������� � �������; 0 - ������ ������, -1  - ������ ������
			dog=_mm_add_pi16(dog,ed);		 //���������� 1 - �������� ����� � �������: 1 - �������� ������ ������, 0 - �������� ������ ������
			//dog=_mm_mullo_pi16(max,dog);	 //�������� ������������ ������ (255) �� �����
			dog=_mm_shuffle_pi8 (dog,mask);  //������ ������������ �������� �� ������ 4 �������
			_mm_stream_pi ((__m64*)(Gray+x+y*BMPinf.Width), dog); // ������ � Gray
		}

}

void BMPImage::Closing()
{
	//�������� - ������������������ ����������� -> �������
	int mas1[25]={0,0,1,0,0,
				 0,1,1,1,0,
				 1,1,1,1,1,
				 0,1,1,1,0,
				 0,0,1,0,0};

	int mas[9]= {1,1,1,
				 1,1,1,
				 1,1,1};
	int n=3,sum=0;
	for (int i=0;i<n*n;++i) if (mas[i]>0) ++sum;
	//sum*=255;
	int size=n/2;
	COLOR*	Buf;
	Buf= (COLOR*)_mm_malloc(count,16);
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;++x)
		{
			int s=0;
			for (int d=-size;d<size+1;++d)
				for (int c=-size;c<size+1;++c)
					s+=Gray[x+(y+d)*BMPinf.Width+c]*mas[c+n*(d+size)+size];
			if (s>0) Buf[x+y*BMPinf.Width]=1; else Buf[x+y*BMPinf.Width]=0;
		}
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;++x)
		{
			int s=0;
			for (int d=-size;d<size+1;++d)
				for (int c=-size;c<size+1;++c)
					s+=Buf[x+(y+d)*BMPinf.Width+c]*mas[c+n*(d+size)+size];
			/*for (int c=-size;c<size+1;++c)
			{
				s+=Buf[x+(y-2)*BMPinf.Width+c]*mas[c+2];
				s+=Buf[x+(y-1)*BMPinf.Width+c]*mas[c+7];
				s+=Buf[x+(y)*BMPinf.Width+c]*mas[c+12];
				s+=Buf[x+(y+1)*BMPinf.Width+c]*mas[c+17];
				s+=Buf[x+(y+2)*BMPinf.Width+c]*mas[c+22];
			}*/
			if (s<sum) Gray[x+y*BMPinf.Width]=0; else Gray[x+y*BMPinf.Width]=1;
			//Gray[x+y*BMPinf.Width]=Buf[x+y*BMPinf.Width];
		}
}

void BMPImage::Closing_simd()
{
	const int n=3;
	__m128i zero=_mm_setzero_si128(),ed=_mm_set1_epi8(1);
	__m128i* mas8[n*n];
    /*int mas1[n*n]={0,0,1,0,0,
				  0,1,1,1,0,
				  1,1,1,1,1,
				  0,1,1,1,0,
				  0,0,1,0,0};
	int mas[n*n]={0,0,0,0,0,
				  0,0,1,0,0,
				  0,1,1,1,0,
				  0,0,1,0,0,
				  0,0,0,0,0};*/
	int mas[n*n]={0,1,0,
				  1,1,1,
				  0,1,0};
	int sum=0;
	for (int i=0;i<n*n;++i)
		if (mas[i]==0) mas8[i]=&zero; else 
		{
			++sum;
			mas8[i]=&ed;
		}
	__m128i sum8=_mm_set1_epi8(sum);
	int size=n/2;
	COLOR*	Buf;
	Buf= (COLOR*)_mm_malloc(count,16); 
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;x+=sizeof(__m128i))
		{
			__m128i s=_mm_setzero_si128();
			for (int d=-size;d<size+1;++d)
				for (int c=-size;c<size+1;++c)
				{
					__m128i gray16=_mm_loadu_si128((__m128i*)(Gray+x+(y+d)*BMPinf.Width+c));		//��������
					gray16=_mm_and_si128(gray16,*mas8[c+n*(d+size)+size]);						    //���������� ���������
					s=_mm_add_epi8(s,gray16);														//�����
				}
			//s=_mm_cmpgt_epi8(s,zero);��� ������ 255 ��������
			s=_mm_add_epi8(_mm_cmpgt_epi8(ed,s),ed);
			_mm_storeu_si128((__m128i*)(Buf+x+y*BMPinf.Width),s);
		}
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;x+=16)
		{
			__m128i s=_mm_setzero_si128();
			for (int d=-size;d<size+1;++d)
				for (int c=-size;c<size+1;++c)
				{
					__m128i gray16=_mm_loadu_si128((__m128i*)(Buf+x+(y+d)*BMPinf.Width+c));		//��������
					gray16=_mm_and_si128(gray16,*mas8[c+n*(d+size)+size]);						//���������� ���������
					s=_mm_add_epi8(s,gray16);													//�����
				}
			s=_mm_add_epi8(_mm_cmpgt_epi8(sum8,s),ed);
			_mm_storeu_si128((__m128i*)(Gray+x+y*BMPinf.Width),s);
		}
	/*for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;x++)
		{
			if (Gray[x+y*BMPinf.Width]==1) Gray[x+y*BMPinf.Width]=255;
		}*/
//		
}

void BMPImage::Filling()
{
	int s,size=7;
	int hsize=(size+1)/2;
	for (int y=4;y<BMPinf.Height-3;++y)
	{
		for (int x=4;x<BMPinf.Width-3;++x)
		{
			s=0;
			for (int j=-hsize;j<hsize;++j)
			{
				s+=Gray[x+(y-hsize)*BMPinf.Width+j];
				s+=Gray[x+(y+hsize-1)*BMPinf.Width+j];
			}
			for (int j=-hsize+1;j<hsize-1;++j)
			{
				s+=Gray[x+(y+j)*BMPinf.Width-hsize  ];
				s+=Gray[x+(y+j)*BMPinf.Width+hsize-1];
			}	
			if (s==0)
				for (int j=-hsize+1;j<hsize-1;++j)
					for(int i=-hsize+1;i<hsize-1;++i)
					{
						Gray[x+i+(y+j)*BMPinf.Width]=0;
					}				
		}
	}
	for (int y=4;y<BMPinf.Height-3;++y) 
			for (int x=4;x<BMPinf.Width-3;++x)
			{
				if (Gray[x+y*BMPinf.Width]==1) Gray[x+y*BMPinf.Width]=255;
			}
}

void BMPImage::Filling_simd() // �� ��������
{
	
	int size=7;
	int hsize=(size+1)/2;
	__m128i s,ed=_mm_set1_epi8(1);
	for (int y=4;y<BMPinf.Height-3;++y)
	{
		for (int x=4;x<BMPinf.Width-3;x+=sizeof(__m128i))
		{
			s=_mm_setzero_si128();
			for (int j=-hsize;j<hsize;++j)
			{
				//s+=Gray[x+(y-hsize)*BMPinf.Width+j];
				//s+=Gray[x+(y+hsize-1)*BMPinf.Width+j];
				s=_mm_add_epi8(s,_mm_loadu_si128((__m128i*)(Gray+x+(y-hsize)*BMPinf.Width+j)));
				s=_mm_add_epi8(s,_mm_loadu_si128((__m128i*)(Gray+x+(y+hsize-1)*BMPinf.Width+j)));
			}
			for (int j=-hsize+1;j<hsize-1;++j)
			{
				//s+=Gray[x+(y+j)*BMPinf.Width-hsize  ];
				//s+=Gray[x+(y+j)*BMPinf.Width+hsize-1];
				s=_mm_add_epi8(s,_mm_loadu_si128((__m128i*)(Gray+x+(y+j)*BMPinf.Width-hsize)));
				s=_mm_add_epi8(s,_mm_loadu_si128((__m128i*)(Gray+x+(y+j)*BMPinf.Width+hsize-1)));
			}	
			//if (s==0)
			//	for (int j=-hsize+1;j<hsize-1;++j)
			//		for(int i=-hsize+1;i<hsize-1;++i)
			//		{
			//			Gray[x+i+(y+j)*BMPinf.Width]=0;
			//		}				
		}
	}
	for (int y=4;y<BMPinf.Height-3;++y) 
			for (int x=4;x<BMPinf.Width-3;++x)
			{
				if (Gray[x+y*BMPinf.Width]==1) Gray[x+y*BMPinf.Width]=255;
			}
}

void BMPImage::Blending()
{
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;++x)
		{
			Red[x+y*BMPinf.Width]*=Gray[x+y*BMPinf.Width];
			Blue[x+y*BMPinf.Width]*=Gray[x+y*BMPinf.Width];
			Green[x+y*BMPinf.Width]*=Gray[x+y*BMPinf.Width];
		}
}

void BMPImage::Blending_simd()
{
	__m128i buf,gr;
	__m128i byteEd=_mm_set1_epi8(255); // ������������� ������ 111..11
	for (int y=4;y<BMPinf.Height-3;++y)
		for (int x=4;x<BMPinf.Width-3;x+=sizeof(__m128i))
		{
			gr=_mm_loadu_si128((__m128i*)(Gray+x+y*BMPinf.Width));
			gr=_mm_xor_si128(gr,byteEd);												//xor c 111..11 �� ��� �������� not
			buf=_mm_loadu_si128((__m128i*)(Blue+x+y*BMPinf.Width));
			_mm_storeu_si128((__m128i*)(Blue+x+y*BMPinf.Width),_mm_or_si128(buf,gr));	//���������� ��������
			buf=_mm_loadu_si128((__m128i*)(Green+x+y*BMPinf.Width));
			_mm_storeu_si128((__m128i*)(Green+x+y*BMPinf.Width),_mm_or_si128(buf,gr));
			buf=_mm_loadu_si128((__m128i*)(Red+x+y*BMPinf.Width));
			_mm_storeu_si128((__m128i*)(Red+x+y*BMPinf.Width),_mm_or_si128(buf,gr));
		}
}

int main()
{
	QueryPerformanceFrequency(&frequency);
	setlocale( LC_ALL,"Russian" );
	char* Path="D:\\car.bmp";
	BMPImage Image1(Path);
	
	QueryPerformanceCounter(&start);
	clock_t start1=GetTickCount();
	//Image1.GetGray();
	Image1.GetGray_simd();
	//Image1.BMPWrite("gray");
	Image1.GlobalContrastCorrection();
	//Image1.BMPWrite("contrast");
	Image1.IntegralImage();
	//Image1.IntegralImage_simd();
    //Image1.DoG();
	Image1.DoG_simd();
	//Image1.BMPWrite("DoG");
	//Image1.Closing();
	Image1.Closing_simd();
	Image1.Filling();
	//Image1.BMPWrite("fill");
	//Image1.Blending();
	Image1.Blending_simd();
	clock_t end1=GetTickCount();
	QueryPerformanceCounter(&finish);
	cout<<"����� ������ GTC = "<<end1-start1 <<" ms"<<endl;
	//Image1.BMPWrite("simd12");
	
	
	int t= (__int64)(finish.QuadPart - start.QuadPart);
	int qp=(__int64)(frequency.QuadPart);
	double delay=(double)(t)/qp*1000.0;
	//double delay2=end1-start1;
	cout<<"����� ������ � = "<<t << " / " <<qp<< " ms"<<endl;
	//cout<<"����� ������ � = "<<delay<< " ms"<<endl;
	//Image1.GausBlur();
	
	system("pause");
	return 0;
}

/*buf1=_mm_loadu_si128((__m128i*)(IntImage+x+(y+1)*BMPinf.Width+1));
			buf2=_mm_loadu_si128((__m128i*)(IntImage+x+(y+1)*BMPinf.Width-2));
			buf3=_mm_loadu_si128((__m128i*)(IntImage+x+(y-2)*BMPinf.Width-2));
			buf4=_mm_loadu_si128((__m128i*)(IntImage+x+(y-2)*BMPinf.Width+1));
			sub1=_mm_add_epi32(buf1,buf3);
			sub2=_mm_sub_epi16(sub1,buf2);
			add1=_mm_sub_epi16(sub1,buf4); */
