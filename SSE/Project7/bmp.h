typedef unsigned short BYTES2;	// ��� ������������ 2 �����
typedef unsigned long  BYTES4;	// ��� ������������ 4 �����
#define _OREAD     0x0000  // ������� ��� ������
#define _OCREATE   0x0100  // ������� � �������
#define _OBINARY   0x8000  // ��� �������� ����
#define _OWRITE    0x0001  // ������� ��� ������
#define _OTRUNC    0x0200  // ������� ��� ��������

struct bmpheader
{
	BYTES2  Type;			//BM
	BYTES4  Size;			//������ � ������
	BYTES2  Reserved1;		//������1
	BYTES2  Reserved2;		//������2
	BYTES4  OffBits;		//��������,� �������� ���������� �����������		
};

struct bmpinfo
{
	BYTES4	Size;			//��������� = 40
	BYTES4	Width;			//������ � ��������
	BYTES4  Height;			//������ � ��������
	BYTES2  Planes;			//����� ���������� = 1
	BYTES2  BitCount;		//��� �� ������� 1,4,8,24
	BYTES4  Compression;	//��� ������ = 0
	BYTES4  SizeImage;		//������ ������� ����������� 
	BYTES4  XPelsPerMeter;	//���������� ��������������
	BYTES4  YPelsPerMeter;	//���������� ������������
	BYTES4  ClrUsed;		//���������� ������������ ������
	BYTES4  ClrImportant;	//���������� ������ ������
};

typedef unsigned char  COLOR; //8bpp 	// ��� ����� 1 ����

struct BGR //24bpp
{
	COLOR BLUE;
	COLOR GREEN;
	COLOR RED;
};

struct BGRR //32bpp
{
	COLOR BLUE;
	COLOR GREEN;
	COLOR RED;
	COLOR RESERVED;
};

