// OpenMP_project.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include "CImg.h"
#include <math.h>
#include <omp.h> 
#include <chrono>
using namespace cimg_library;
using namespace std;
#define NUM_THREADS 8//���������� �������
#define NUM_MEAN_1 5 //������ ����� �������� ������� ��� ��������
#define NUM_MEAN_2 3 //������ ����� �������� ������� ��� ��������
#define STEP 2   //����� (���������) 2 ��� � �������
#define AREA 20 //����� ��������� �� �������

class Stack//��� ��������� FloodFill
{
	int X, Y;
	Stack *Next;
public:
	int GetX() const;
	int GetY() const;
	friend Stack* Push(Stack*, int, int);
	friend Stack* Pop(Stack*);
};

Stack* Push(Stack*, int, int);
Stack* Pop(Stack*);
void Gaussian_filter(unsigned char*, const int, const int, const int, const int, const double);
void AutoContrastGray(unsigned char*, const int, const int, const int, int, int);
void Closing(unsigned char*, const int, const int, const int);
void Opening(unsigned char*, const int, const int, const int);
int FloodFill(unsigned char*, bool*, int, int, const int, const int, bool);
void DoB(unsigned char*, const int, const int, const int);
void DoG(unsigned char*, const int, const int, const int);
void DeleteSS9(unsigned char*, const int, const int, const int);
void DeleteSS7(unsigned char*, const int, const int, const int);
void SaveImage(unsigned char*, unsigned char*, unsigned char*, const int, const int, const int, const char*);

int _tmain(int argc, _TCHAR* argv[])
{
#pragma omp parallel num_threads(NUM_THREADS)
	cout << "Hello" << endl;

	const char* imageFileName = "D:\\pictures\\Picture1.bmp";
	CImg<unsigned char> image(imageFileName);//�������� �����������
	const int H = image.height();
	const int W = image.width();

	const int L = H*W;//���������� �������� �����������
	unsigned char* grayscale_image = new unsigned char[L];//����������� � ��������� ������
	bool* Ind = new bool[L];
	unsigned char* R = new unsigned char[L];
	unsigned char* G = new unsigned char[L];
	unsigned char* B = new unsigned char[L];
	memset(Ind, false, L);//��������� ������ Ind ���������� false

	LARGE_INTEGER frequency, start, finish;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y<H; y++)
		for (int x = 0; x<W; x++)
		{
			int r = image(x, y, 0);
			int g = image(x, y, 1);
			int b = image(x, y, 2);
			R[y*W + x] = r;
			G[y*W + x] = g;
			B[y*W + x] = b;
			grayscale_image[y*W + x] = (int)(0.3*r + 0.6*g + 0.1*b);//��������� � �������� ������
		}

	//SaveImage(grayscale_image, grayscale_image, grayscale_image, H, W, L, "D:\\image_grayscale.bmp");//�������� �����������


	AutoContrastGray(grayscale_image, H, W, L, 3000, 1000);//��������������������
	//DoG(grayscale_image, H, W, L);//���� �������
	
	DoB(grayscale_image, H, W, L);//���� �������
	Closing(grayscale_image,H,W,L);//�������� �������� (������� ������� �� �������� ��������)
	//SaveImage(grayscale_image, grayscale_image, grayscale_image, H, W, L, "D:\\image_DoB.bmp");//�������� �����������
	
	/*
	//�������� ������� �������� � ��������� ��������
	int** small_S=new int*[L];//������ ��������� ����� ��� �������� ����� �������
	for (int i=0;i<L;i++)
	{
	small_S[i]=new int[2];
	}
	int num_S=0;//������� ��� ����� ��������
	for (int y=0;y<H;y++)//��� ���� �������� �����������
	for (int x=0;x<W;x++)
	if (!(Ind[y*W+x]) && grayscale_image[y*W+x]!=0)//��������� �����
	{
	int S=FloodFill(grayscale_image,Ind,x,y,H,W,false);//������� ������� ��������� � (�,�) �������
	if (S<AREA)
	{
	small_S[num_S][0]=x;//���������� ���������� ��������� �����
	small_S[num_S][1]=y;
	num_S++;
	}
	}
	memset(Ind,false,L);//��������� ������ Ind ���������� false
	for (int i=0;i<num_S;i++)
	int S=FloodFill(grayscale_image,Ind,small_S[i][0],small_S[i][1],H,W,true);//S �� �����, ����������� �������

	for (int i = 0; i<L; i++)
	{
	delete[] small_S[i];
	small_S[i] = nullptr;
	}
	delete[] small_S;
	small_S = nullptr;
	///
	*/

	DeleteSS9(grayscale_image,H,W,L);

	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i<L; i++)
		{
			if (grayscale_image[i] == 0)
			{
				R[i] = 255;
				G[i] = 255;
				B[i] = 255;
			}
		}

	QueryPerformanceCounter(&finish);
	// ����� � �������������
	float delay = (finish.QuadPart - start.QuadPart) * 1000.0f / frequency.QuadPart;
	cout << "QueryPerformanceCounter = " << delay << " ms\n";

	SaveImage(R, G, B, H, W, L, "D:\\image_result.bmp");//�������� �����������
	delete[] grayscale_image;
	grayscale_image = nullptr;
	///
	delete[] Ind;
	Ind = nullptr;
	///
	delete[] R;
	R = nullptr;
	///
	delete[] G;
	G = nullptr;
	///
	delete[] B;
	B = nullptr;

	system("pause");
	return 0;
}
//-----------------------------------------------------------------------------------------
//                               ������ ������
//-----------------------------------------------------------------------------------------
void Gaussian_filter(unsigned char *matrix, const int H, const int W, const int L, const int SIZE, const double SIGMA)
{
	//������� ������, ��� ����� ��������� ����������
	int *temp = new int[L];

	//���������� ����� ������� ������
	double** mask = new double*[SIZE];
	for (int i = 0; i<SIZE; i++)
		mask[i] = new double[SIZE];
	//��������� ����� ������� ������
	int half_size = SIZE / 2;
	double sigma2 = SIGMA*SIGMA;
	for (int i = -half_size; i <= half_size; i++)
		for (int j = -half_size; j <= half_size; j++)
		{
			if (i == 0 && j == 0)//��������
				mask[i + half_size][j + half_size] = 1;
			else
				mask[i + half_size][j + half_size] = exp(-1.0*(i*i + j*j) / (2 * sigma2));
		}


	double sum = 0.0;//���������� ����� ��������� �����, ����� �����������
	for (int i = 0; i<SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			sum += mask[i][j];


	//��������� �����
	double s = 0.0;
	#pragma omp parallel for num_threads(NUM_THREADS) reduction(+ : s)
	for (int y = half_size; y<H - half_size; y++)//��� ���� �������� �����������
		for (int x = half_size; x<W - half_size; x++)
		{
			for (int i = -half_size; i <= half_size; i++)//��� ���� ��������� �����
				for (int j = -half_size; j <= half_size; j++)
				{
					double M = mask[i + half_size][j + half_size];
					s += matrix[(y + j)*W + (x + i)] * M;
				}
			temp[y*W + x] = (int)(s / sum)>255 ? 255 : (int)(s / sum);
			s = 0;
		}

	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i<L; i++)
	{
		matrix[i] = temp[i];
	}


	//������� ��������
	delete[] temp;
	temp = nullptr;
	///
	for (int i = 0; i<SIZE; i++)
	{
		delete[] mask[i];
		mask[i] = nullptr;
	}
	delete[] mask;
	mask = nullptr;
}
//-----------------------------------------------------------------------------------------
//                    ��������� ����������� ��������� �����������                         
//-----------------------------------------------------------------------------------------
void AutoContrastGray(unsigned char* Matrix, const int H, const int W, const int L, int HT, int CT)
{

	unsigned long alHist[256], lT = 120, lSum;
	unsigned char abLUT[256];
	int i, lLeftLim, lRightLim;
	// ���������� ����������� ��������
	memset(alHist, 0, 256 * sizeof(long));
	unsigned char *pb = Matrix;
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			alHist[pb[y*W + x]]++;
		}

	// ���������� ����� ������� ��� ����������������
	lSum = 0;
	for (lLeftLim = 0; lLeftLim < 100; lLeftLim++)
	{

		if (alHist[lLeftLim] > HT) break;
		lSum += alHist[lLeftLim];

		if (lSum > CT) break;
	}
	// ���������� ������ ������� ��� ����������������
	lSum = 0;
	for (lRightLim = 255; lRightLim > 150; lRightLim--)
	{
		if (alHist[lRightLim] > lT) break;
		lSum += alHist[lRightLim];
		if (lSum > lT) break;
	}
	// ���������� ������� ������������� (LUT, Look-Up Table)
	for (i = 0; i < lLeftLim; i++) { abLUT[i] = (unsigned char)0; }
	for (i = lLeftLim; i < lRightLim; i++)
	{
		abLUT[i] = (unsigned char)(255 * (i - lLeftLim) / (lRightLim - lLeftLim));
	}
	for (i = lRightLim; i < 256; i++) { abLUT[i] = (unsigned char)255; }
	// ��������������� ����������������
	//pb = Matrix;
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			pb[y*W + x] = abLUT[pb[y*W + x]];
		}
}
//-----------------------------------------------------------------------------------------
//                             ������� ��������
//-----------------------------------------------------------------------------------------
void DoG(unsigned char* matrix1, const int H, const int W, const int L)
{

	unsigned char* matrix2 = new unsigned char[L];//����� ������� matrix 1
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i < L; i++)
		matrix2[i] = matrix1[i];

	//�������� ������ ������ � ��������
	Gaussian_filter(matrix1, H, W, L, 5, 1);
	Gaussian_filter(matrix2, H, W, L, 5, 1.6);

	//��������� ������
	unsigned char* temp = new unsigned char[L];
	memset(temp, 0, L);//��������� ������ ������

	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)//�������� �� ����� �����������
		for (int x = 0; x < W; x++)
		{
			int p = matrix2[y*W + x] - matrix1[y*W + x];
			if (p<0) p = 0;
			if (p>255) p = 255;
			temp[y*W + x] = p>STEP? 255 : 0;
		}

	//��������� ����������� �������� � �������� ������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i < L; i++)
		matrix1[i] = temp[i];

	delete[] matrix2;
	matrix2 = nullptr;
	///
	delete[] temp;
	temp = nullptr;

}
//-----------------------------------------------------------------------------------------
//                             ������� �������
//-----------------------------------------------------------------------------------------
void DoB(unsigned char* Matrix, const int H, const int W, const int L)
{

	int *integral = new int[L];
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int i = 0; i<L; i++)
		integral[i] = (int)Matrix[i]; //�������� �� Matrix � integral
	//��������� ������������ �����������
	// ����� �� ���������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int x = 0; x < W; x++)
		for (int y = 1; y < H; y++)
		{
			integral[y*W + x] += integral[(y - 1)*W + x];
		}
	//����� �� �������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)
		for (int x = 1; x < W; x++)
		{
			integral[y*W + x] += integral[y*W + (x - 1)];
		}

	//������� �������
	double* first_mean = new double[L];//���������� ������� ������ ������
	double* second_mean = new double[L];

	int hs_1 = NUM_MEAN_1 / 2;//�������� �� ������� ����� (������������� �������)
	int hs_2 = NUM_MEAN_2 / 2;
	//������ �������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = hs_1; y<H - hs_1; y++)//��� ���� �������� �����������
		for (int x = hs_1; x<W - hs_1; x++)
		{
			int sum = 0;
			if (y - hs_1 >= 1 && x - hs_1 >= 1)
				sum = integral[(y + hs_1)*W + (x + hs_1)] + integral[(y - hs_1 - 1)*W + (x - hs_1 - 1)] - integral[(y + hs_1)*W + (x - hs_1 - 1)]
				- integral[(y - hs_1 - 1)*W + (x + hs_1)];
			else
				sum = integral[(y + hs_1)*W + (x + hs_1)];
			double num1 = NUM_MEAN_1*NUM_MEAN_1;
			first_mean[y*W + x] = (double)sum / num1;
		}
	//������ �������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = hs_2; y<H - hs_2; y++)//��� ���� �������� �����������
		for (int x = hs_2; x<W - hs_2; x++)
		{
			int sum = 0;
			if (y - hs_2 >= 1 && x - hs_2 >= 1)
				sum = integral[(y + hs_2)*W + (x + hs_2)] + integral[(y - hs_2 - 1)*W + (x - hs_2 - 1)] - integral[(y + hs_2)*W + (x - hs_2 - 1)]
				- integral[(y - hs_2 - 1)*W + (x + hs_2)];
			else
				sum = integral[(y + hs_2)*W + (x + hs_2)];
			double num2 = NUM_MEAN_2*NUM_MEAN_2;
			second_mean[y*W + x] = (double)sum / num2;
		}
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)//�������� �� ����� �����������
		for (int x = 0; x < W; x++)
		{
			int p = second_mean[y*W + x] - first_mean[y*W + x];
			if (p<0) p = 0;
			if (p>255) p = 255;
			Matrix[y*W + x] = p>STEP ? 255 : 0;
		}

	//������� ��������
	delete[] first_mean;
	first_mean = nullptr;
	//////
	delete[] second_mean;
	second_mean = nullptr;
	//////
	delete[] integral;
	integral = nullptr;
}
//-----------------------------------------------------------------------------------------
//                             �������� ��������
//-----------------------------------------------------------------------------------------
void Closing(unsigned char* Matrix, const int H, const int W, const int L)
{
	unsigned char* temp_arr = new unsigned char[L];
	memset(temp_arr, 0, L);//temp_arr[:]=0
	//������ 3�3
	//���������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 1; y<H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (Matrix[y*W + x] == 255)
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						if (l == 0 || t == 0)
							temp_arr[(y + l)*W + (x + t)] = 255;//������� ��������� �� ��������� ������
		}

	//������
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 1; y < H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (temp_arr[y*W + x] == 0)
				Matrix[y*W + x] = 0;
			else
			{
				Matrix[y*W + x] = 255;
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						if (l == 0 || t == 0)
							if (temp_arr[(y + l)*W + (x + t)] == 0)
							{
								Matrix[y*W + x] = 0;
								break;
							}
			}
		}

	//������� ��������
	delete[] temp_arr;
	temp_arr = nullptr;
}
//-----------------------------------------------------------------------------------------
//                             ������� ��������
//-----------------------------------------------------------------------------------------
int FloodFill(unsigned char* Matrix, bool* Ind, int XStart, int YStart, const int H, const int W, bool fill)//���������� �������
{
	int S = 0;//��� ����������� �������
	Stack *Head = Push(nullptr, XStart, YStart); //������� ���� 
	while (Head)
	{
		int X = Head->GetX();
		int y = Head->GetY();
		Ind[y*W + X] = true;//��������, ��� �� ����� ����
		S++;//����������� �������
		if (fill)//���� ����� ������ �����
			Matrix[y*W + X] = 0;
		Head = Pop(Head);

		int xl = X - 1;
		int xr = X + 1;

		if (Ind[y*W + xl])
			continue;

		//��������� � ����� �����
		while (xl >= 0 && (Matrix[y*W + xl] != 0 && !(Ind[y*W + xl])))//�� ������� � �� ���� ����������������
		{
			Ind[y*W + xl] = true;
			S++;
			if (fill)//���� ����� ������ �����
				Matrix[y*W + xl] = 0;
			xl--;
		}
		++xl;   //������������ � ���������� �������

		//��������� � ������ �����
		while (xr < W && Matrix[y*W + xr] != 0 && !(Ind[y*W + xr]))
		{
			Ind[y*W + xr] = true;
			S++;
			if (fill)//���� ����� ������ �����
				Matrix[y*W + xr] = 0;
			xr++;
		}
		--xr;

		//��������� ������� ����� (���� ��� ����������)
		bool bLeftBack = true;
		if (y > 0)
			for (int x = xl; x <= xr; ++x)
			{
				if (Matrix[(y - 1)*W + x] != 0 && !(Ind[(y - 1)*W + x]))
				{
					if (bLeftBack)
						Head = Push(Head, x, y - 1);
					bLeftBack = false;
				}
				else
					bLeftBack = true;
			}
		//��������� ������ ����� (���� ��� ����������)
		bLeftBack = true;
		if (y < H)
			for (int x = xl; x <= xr; ++x)
			{
				// check for being out of zone
				if (Matrix[(y + 1)*W + x] != 0 && !(Ind[(y + 1)*W + x]))
				{
					if (bLeftBack)
						Head = Push(Head, x, y + 1);
					bLeftBack = false;
				}
				else
					bLeftBack = true;
			}
	}
	return S;
}
//-----------------------------------------------------------------------------------------
//                             ���������� �������� � ����
//-----------------------------------------------------------------------------------------
Stack *Push(Stack *head, int Xnew, int Ynew)
{
	Stack *NewElement = new Stack;
	NewElement->Next = head;
	NewElement->X = Xnew;
	NewElement->Y = Ynew;
	return NewElement;
}
//-----------------------------------------------------------------------------------------
//                             ������� �������� �� �����
//-----------------------------------------------------------------------------------------
Stack *Pop(Stack *head)
{
	if (head)
	{
		Stack *pElement = head->Next;
		delete head;
		return pElement;
	}
	return nullptr;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetX
//-----------------------------------------------------------------------------------------
int Stack::GetX() const
{
	return X;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetY
//-----------------------------------------------------------------------------------------
int Stack::GetY() const
{
	return Y;
}
//-----------------------------------------------------------------------------------------
//                             �������� ��������
//-----------------------------------------------------------------------------------------
void Opening(unsigned char* Matrix, const int H, const int W, const int L)
{
	unsigned char* temp_arr = new unsigned char[L];
	memset(temp_arr, 0, L);//temp_arr[:]=0
	//������ 3�3
	//������
	//#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 1; y<H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (Matrix[y*W + x] == 0)
				temp_arr[y*W + x] = 0;
			else
			{
				temp_arr[y*W + x] = 255;
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						if (Matrix[(y + l)*W + (x + t)] == 0)
						{
							temp_arr[y*W + x] = 0;
							break;
						}
			}
		}
	memset(Matrix, 0, L);
	//���������
	//#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 1; y<H - 1; y++)
		for (int x = 1; x < W - 1; x++)
		{
			if (temp_arr[y*W + x] == 255)
				for (int l = -1; l < 2; l++)
					for (int t = -1; t < 2; t++)
						Matrix[(y + l)*W + (x + t)] = 255;//������� ��������� �� �������� ������
		}
	//������� ��������
	delete[] temp_arr;
	temp_arr = nullptr;
}
//-----------------------------------------------------------------------------------------
//                         �������� ��������� �������� ���������� �����
//-----------------------------------------------------------------------------------------
void DeleteSS9(unsigned char* Matrix, const int H, const int W, const int L)
{
	//������ �����=9 � 9
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 4; y<H - 4; y++)
		for (int x = 4; x < W - 4; x++)
		{
			bool flag = false;
			for (int l = -4; l < 5; l++)
			{
				if (Matrix[(y + l)*W + (x + 4)] != 0)
					flag = true;
				if (Matrix[(y + l)*W + (x - 4)] != 0)
					flag = true;
				if (Matrix[(y + 4)*W + (x + l)] != 0)
					flag = true;
				if (Matrix[(y - 4)*W + (x + l)] != 0)
					flag = true;
			}
			if (!flag)
				for (int l = -4; l < 5; l++)
					for (int t = -4; t < 5; t++)
						Matrix[(y + l)*W + (x + t)] = 0;
		}
}
//-----------------------------------------------------------------------------------------
//                         �������� ��������� �������� ���������� �����
//-----------------------------------------------------------------------------------------
void DeleteSS7(unsigned char* Matrix, const int H, const int W, const int L)
{

	//������ �����=9 � 9
	#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 3; y<H - 3; y++)
		for (int x = 3; x < W - 3; x++)
		{
			bool flag = false;
			for (int l = -3; l < 4; l++)
			{
				if (Matrix[(y + l)*W + (x + 3)] != 0)
					flag = true;
				if (Matrix[(y + l)*W + (x - 3)] != 0)
					flag = true;
				if (Matrix[(y + 3)*W + (x + l)] != 0)
					flag = true;
				if (Matrix[(y - 3)*W + (x + l)] != 0)
					flag = true;
			}
			if (!flag)
				for (int l = -3; l < 4; l++)
					for (int t = -3; t < 4; t++)
						Matrix[(y + l)*W + (x + t)] = 0;
		}
}
//-----------------------------------------------------------------------------------------
//                         ���������� �����������
//-----------------------------------------------------------------------------------------
void SaveImage(unsigned char *matrixR, unsigned char *matrixG, unsigned char *matrixB, const int H, const int W, const int L, const char* string)
{
	CImg<unsigned char> imageFilter(W, H, 1, 3);//������� ����������� � ����������� ���������
	//#pragma omp parallel for num_threads(NUM_THREADS)
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
		{
			const unsigned char Color[] = { matrixR[y*W + x], matrixG[y*W + x], matrixB[y*W + x] };//������� ���� ������� (x,y)
			imageFilter.draw_point(x, y, Color);//����������� ������� ������ ����������� ���� ������
		}

	imageFilter.save(string);
}
